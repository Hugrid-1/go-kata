package easy

/*
	runtime 0ms 100%
	memory 2.1 mb 20.75%
*/
func numJewelsInStones(jewels string, stones string) int {
	jewelsMap := make(map[int32]uint8, 50)
	countJewels := 0
	for _, ch := range jewels {
		jewelsMap[ch] = 1
	}
	for _, ch := range stones {
		if _, ok := jewelsMap[ch]; ok {
			countJewels++
		}
	}
	return countJewels
}
