package main

import (
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"io"
	"log"
	"math/rand"
	"os"
	"time"
)

var EmptyListError = fmt.Errorf("list is empty")

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Insert(n int, c Commit)
	Delete(n int)
	Index() int
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
	LoadData(path string) error
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("error opening file: err:", err)
		return err
	}
	var commits []Commit

	data, err := io.ReadAll(file)
	if err != nil {
		fmt.Println("error reading file: err:", err)
		return err
	}

	err = json.Unmarshal(data, &commits)
	if err != nil {
		fmt.Println("error unmarshal json: err:", err)
		return err
	}

	commits = quickSortCommit(commits)
	d.head = &Node{
		data: &commits[0],
		prev: nil,
		next: nil,
	}
	d.curr = d.head
	d.tail = d.head
	d.len++
	for i := 1; i < len(commits); i++ {
		newNode := &Node{
			data: &commits[i],
			prev: d.tail,
			next: nil,
		}
		d.tail.next = newNode
		d.tail = newNode
		d.len++
	}

	return err
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	if d.head == nil {
		return nil
	}
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.head == nil {
		return nil
	}

	if d.curr.next == nil {
		return nil
	}

	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.head == nil {
		return nil
	}

	if d.curr.prev == nil {
		return nil
	}

	d.curr = d.curr.prev
	return d.curr

}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	newNode := &Node{
		data: &c,
	}
	if d.len == 0 || d.head == nil {
		d.head = newNode
		d.curr = newNode
		d.tail = newNode
		d.len++
		return nil
	}

	if n >= d.len-1 {
		newNode.prev = d.tail
		d.tail.next = newNode
		d.tail = newNode
		d.len++
		return nil
	} else if n <= 0 {
		newNode.next = d.head
		d.head.prev = newNode
		d.head = newNode
		d.len++
		return nil
	}
	idx, err := d.Index()
	if err != nil {
		return EmptyListError
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}

	beforeNode := d.curr
	nextNode := beforeNode.next

	newNode.prev = beforeNode
	newNode.next = nextNode

	beforeNode.next = newNode
	nextNode.prev = newNode
	d.len++
	return err
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	err = d.DeleteCurrent()
	if err != nil {
		return err
	}
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 0 {
		return EmptyListError
	}
	if d.curr == d.head {
		_, err := d.Shift()
		if err != nil {
			return err
		}
		return nil
	} else if d.curr == d.tail {
		_, err := d.Pop()
		if err != nil {
			return err
		}
		return nil
	}
	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev
	tmp := d.curr.prev
	d.curr.next = nil
	d.curr.prev = nil
	d.curr = tmp
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.head == nil {
		return 0, EmptyListError
	}
	element := d.curr
	counter := 0
	for element.prev != nil {
		counter++
		element = element.prev
	}
	return counter, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() (*Node, error) {
	deletedNode := d.tail
	if d.tail == nil || d.head == nil {
		return deletedNode, fmt.Errorf("list is empty or tail element is nil")
	}

	if d.tail == d.head {
		d.tail = nil
		d.head = nil
		d.curr = nil
	} else if d.tail == d.curr {
		d.tail.prev.next = nil
		d.tail = d.tail.prev
		d.curr = d.tail
	} else {
		d.tail.prev.next = nil
		d.tail = d.tail.prev
	}

	d.len--
	return deletedNode, nil
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() (*Node, error) {
	deletedNode := d.head
	if d.head == nil {
		return deletedNode, EmptyListError
	}

	if d.head == d.tail {
		d.tail = nil
		d.head = nil
		d.curr = nil
	} else if d.head == d.curr {
		d.head.next.prev = nil
		d.head = d.head.next
		d.curr = d.head
	} else {
		d.head.next.prev = nil
		d.head = d.head.next
	}

	d.len--
	return deletedNode, nil
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) (*Node, error) {
	if d.len == 0 {
		return nil, EmptyListError
	}
	currentElement := d.head
	for i := 0; i < d.len; i++ {
		if currentElement.data.UUID == uuID {
			return currentElement, nil
		}
		currentElement = currentElement.next
	}
	return nil, fmt.Errorf("not found node with UUID = %v", uuID)
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) (*Node, error) {
	if d.len == 0 {
		return nil, EmptyListError
	}
	currentElement := d.head
	for i := 0; i < d.len; i++ {
		if currentElement.data.Message == message {
			return currentElement, nil
		}
		currentElement = currentElement.next
	}
	return nil, fmt.Errorf("not found node with message = %v", message)
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() (*DoubleLinkedList, error) {
	if d.head == nil {
		return d, EmptyListError
	}
	var nextNode *Node
	currentNode := d.head
	d.head, d.tail = d.tail, d.head
	for currentNode != nil {
		nextNode = currentNode.next
		currentNode.next, currentNode.prev = currentNode.prev, currentNode.next
		currentNode = nextNode
	}

	return d, nil
}

//// String вывод списка в консоль
//func (d *DoubleLinkedList) String() string {
//	var resultString string
//	el := d.head
//	for i := 0; el != nil; i++ {
//		resultString += fmt.Sprintf("ID:%v || UUID:%v | DATE:%v |MESSAGE:%v \n", i, el.data.UUID, el.data.Date, el.data.Message)
//		el = el.next
//	}
//	return resultString
//}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func quickSortCommit(commits []Commit) []Commit {
	if len(commits) <= 1 {
		return commits
	}

	median := commits[rand.Intn(len(commits))]

	lowPart := make([]Commit, 0, len(commits))
	highPart := make([]Commit, 0, len(commits))
	middlePart := make([]Commit, 0, len(commits))

	for _, item := range commits {
		switch {
		case item.Date.Unix() < median.Date.Unix():
			lowPart = append(lowPart, item)
		case item.Date.Unix() == median.Date.Unix():
			middlePart = append(middlePart, item)
		case item.Date.Unix() > median.Date.Unix():
			highPart = append(highPart, item)
		}
	}

	lowPart = quickSortCommit(lowPart)
	highPart = quickSortCommit(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)
	return lowPart
}

func GenerateJSON() (*os.File, int, error) {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(100)
	data := make([]Commit, n)
	for i, commit := range data {
		err := gofakeit.Struct(&commit)
		if err != nil {
			log.Fatal(err)
		}
		data[i] = commit
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
		return nil, 0, err
	}
	jsonFile, err := os.Create("data/generatedData.json")
	if err != nil {
		log.Fatal(err)
		return nil, 0, err
	}
	defer jsonFile.Close()
	_, err = jsonFile.Write(jsonData)
	if err != nil {
		log.Fatal(err)
		return nil, 0, err
	}
	return jsonFile, n, err
}

func main() {
	l := &DoubleLinkedList{}
	//jsonFile, _ := GenerateJSON()
	err := l.LoadData("data/test_small.json")
	if err != nil {
		panic(err)
	}
	//fmt.Println("LEN ", l.Len(), l.String())
	//l.Reverse()
	//fmt.Println("LEN ", l.Len(), l.String())
	//fmt.Println("LEN ", l.Len(), l.String())

}
