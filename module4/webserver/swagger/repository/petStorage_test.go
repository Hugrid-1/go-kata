package repository

import (
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"reflect"
	"sync"
	"testing"
)

type petFields struct {
	data               []*models.Pet
	primaryKeyIDx      map[int]*models.Pet
	autoIncrementCount int
	Mutex              sync.Mutex
}

func getTestPetData() petFields {
	testFields := petFields{
		data: []*models.Pet{
			{
				ID: 0,
				Category: models.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []models.Category{},
				Status: "active",
			},
			{
				ID: 1,
				Category: models.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Filya",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []models.Category{},
				Status: "active",
			},
		},
		primaryKeyIDx: map[int]*models.Pet{
			0: {ID: 0, Category: models.Category{ID: 0, Name: "test category"}, Name: "Alma", PhotoUrls: []string{"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg", "http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg", "https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg"}, Tags: []models.Category{}, Status: "active"},
			1: {ID: 1, Category: models.Category{ID: 0, Name: "test category"}, Name: "Filya", PhotoUrls: []string{"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg", "http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg", "https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg"}, Tags: []models.Category{}, Status: "active"}},
		autoIncrementCount: 2,
		Mutex:              sync.Mutex{},
	}
	return testFields
}

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int64]*models.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: models.Pet{
					ID: 0,
					Category: models.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"http://localhost:8080/swagger",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []models.Category{},
					Status: "active",
				},
			},
			want: models.Pet{
				ID: 0,
				Category: models.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"http://localhost:8080/swagger",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []models.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got models.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	testData := getTestPetData()
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  petFields
		args    args
		wantErr bool
	}{
		{
			name:    "Base test",
			fields:  testData,
			args:    args{0},
			wantErr: false,
		},
		{
			name:    "Base test",
			fields:  testData,
			args:    args{5},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if err := p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	testData := getTestPetData()
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name    string
		fields  petFields
		args    args
		want    models.Pet
		wantErr bool
	}{
		{
			name:   "base test",
			fields: testData,
			args: args{models.Pet{
				ID:        0,
				Category:  models.Category{ID: 0, Name: "test category"},
				Name:      "AlmaUPDATED",
				PhotoUrls: []string{},
				Tags:      []models.Category{},
				Status:    "active"}},
			want: models.Pet{
				ID:        0,
				Category:  models.Category{ID: 0, Name: "test category"},
				Name:      "AlmaUPDATED",
				PhotoUrls: []string{},
				Tags:      []models.Category{},
				Status:    "active"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := p.Update(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetList(t *testing.T) {
	testData := getTestPetData()
	tests := []struct {
		name   string
		fields petFields
		want   []models.Pet
	}{
		{
			name:   "base test",
			fields: testData,
			want: []models.Pet{
				{
					ID: 0,
					Category: models.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []models.Category{},
					Status: "active",
				},
				{
					ID: 1,
					Category: models.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Filya",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []models.Category{},
					Status: "active",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := p.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetByStatus(t *testing.T) {
	testData := getTestPetData()
	type args struct {
		status []string
	}
	tests := []struct {
		name    string
		fields  petFields
		args    args
		want    []models.Pet
		wantErr bool
	}{
		{
			name:   "base tesst",
			fields: testData,
			args:   args{[]string{"active"}},
			want: []models.Pet{
				{
					ID: 0,
					Category: models.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []models.Category{},
					Status: "active",
				},
				{
					ID: 1,
					Category: models.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Filya",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []models.Category{},
					Status: "active",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := p.GetByStatus(tt.args.status...)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByStatus() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_AddPhotoUrl(t *testing.T) {
	testData := getTestPetData()
	type args struct {
		petID    int
		filepath string
	}
	tests := []struct {
		name    string
		fields  petFields
		args    args
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{0, "./module4/webserver/swagger/public/swagger.json"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if err := p.AddPhotoUrl(tt.args.petID, tt.args.filepath); (err != nil) != tt.wantErr {
				t.Errorf("AddPhotoUrl() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
