package test

import (
	"encoding/json"
	"log"
	"os"
	"reflect"
	"testing"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/service/repo"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/service/model"
	service "gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/service/service"
)

type fields struct {
	repository service.TodoRepository
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

/*
На скриншоте в задании есть 2 тестовых файла но т.к. функции репозитория тестируются при вызовые методов сервиса я их решил не создавать отдельный файл
+ в 3 пункте задания указано именно сделать тесты для use case
*/
func createTestRepository() repo.FileTaskRepository {
	filepath := "./testRepository.json"
	testTodoData := []model.Todo{
		{ID: 1, Title: "wash the dishes", Description: "1test description", Status: "not started"},
		{ID: 2, Title: "shopping", Description: "2test description", Status: "not started"},
		{ID: 3, Title: "go to store", Description: "3test description", Status: "finished"},
		{ID: 4, Title: "walk the dog", Description: "4test description", Status: "not started"},
	}
	file, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0666)
	check(err)
	err = file.Truncate(0)
	check(err)
	_, err = file.Seek(0, 0)
	check(err)
	testData, err := json.Marshal(testTodoData)
	check(err)
	_, err = file.Write(testData)
	check(err)
	repository := repo.NewFIleTaskRepository(filepath)
	return *repository
}

func Test_todoService_CreateTodo(t *testing.T) {
	testRepository := createTestRepository()
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "basic test",
			fields:  fields{repository: &testRepository},
			args:    args{title: "test task"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(tt.fields.repository)
			if err := s.CreateTodo(tt.args.title); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_EditTodo(t *testing.T) {
	testRepository := createTestRepository()
	type args struct {
		todo           model.Todo
		newTitle       string
		newDescription string
		newStatus      string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "basic test",
			fields: fields{repository: &testRepository},
			args: args{
				todo:           model.Todo{ID: 1, Title: "wash the dishes", Description: "1test description", Status: "not started"},
				newTitle:       "edittedTItle",
				newDescription: "edittedDescription",
				newStatus:      "finished",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(tt.fields.repository)
			if err := s.EditTodo(tt.args.todo, tt.args.newTitle, tt.args.newDescription, tt.args.newStatus); (err != nil) != tt.wantErr {
				t.Errorf("EditTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_ListTodos(t *testing.T) {
	testRepository := createTestRepository()
	tests := []struct {
		name    string
		fields  fields
		want    []model.Todo
		wantErr bool
	}{
		{
			name:   "basic test",
			fields: fields{repository: &testRepository},
			want: []model.Todo{
				{ID: 1, Title: "wash the dishes", Description: "1test description", Status: "not started"},
				{ID: 2, Title: "shopping", Description: "2test description", Status: "not started"},
				{ID: 3, Title: "go to store", Description: "3test description", Status: "finished"},
				{ID: 4, Title: "walk the dog", Description: "4test description", Status: "not started"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(tt.fields.repository)
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	testRepository := createTestRepository()
	type args struct {
		todo model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "basic test",
			fields:  fields{repository: &testRepository},
			args:    args{model.Todo{ID: 1, Title: "wash the dishes", Description: "1test description", Status: "not started"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(tt.fields.repository)
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
