package main

import (
	"reflect"
	"testing"
)

func TestNewWeatherFacade(t *testing.T) {
	type args struct {
		apiKey string
	}
	tests := []struct {
		name string
		args args
		want WeatherFacade
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewWeatherFacade(tt.args.apiKey); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewWeatherFacade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenWeatherAPI_GetHumidity(t *testing.T) {
	type fields struct {
		apiKey string
	}
	type args struct {
		location string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OpenWeatherAPI{
				apiKey: tt.fields.apiKey,
			}
			if got := o.GetHumidity(tt.args.location); got != tt.want {
				t.Errorf("GetHumidity() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenWeatherAPI_GetTemperature(t *testing.T) {
	type fields struct {
		apiKey string
	}
	type args struct {
		location string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OpenWeatherAPI{
				apiKey: tt.fields.apiKey,
			}
			if got := o.GetTemperature(tt.args.location); got != tt.want {
				t.Errorf("GetTemperature() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenWeatherAPI_GetWindSpeed(t *testing.T) {
	type fields struct {
		apiKey string
	}
	type args struct {
		location string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OpenWeatherAPI{
				apiKey: tt.fields.apiKey,
			}
			if got := o.GetWindSpeed(tt.args.location); got != tt.want {
				t.Errorf("GetWindSpeed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOpenWeatherAPI_getWeatherData(t *testing.T) {
	type fields struct {
		apiKey string
	}
	type args struct {
		location   string
		regionCode string
		limit      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    WeatherData
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OpenWeatherAPI{
				apiKey: tt.fields.apiKey,
			}
			got, err := o.getWeatherData(tt.args.location, tt.args.regionCode, tt.args.limit)
			if (err != nil) != tt.wantErr {
				t.Errorf("getWeatherData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getWeatherData() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWeatherFacade_GetWeatherInfo(t *testing.T) {
	type fields struct {
		weatherAPI WeatherAPI
	}
	type args struct {
		location string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
		want1  int
		want2  int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &WeatherFacade{
				weatherAPI: tt.fields.weatherAPI,
			}
			got, got1, got2 := w.GetWeatherInfo(tt.args.location)
			if got != tt.want {
				t.Errorf("GetWeatherInfo() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GetWeatherInfo() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("GetWeatherInfo() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}
