package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	m := make(map[string]int)
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 12300,
		},
		{
			Name:  "https://github.com/docker/cli-docs-tool",
			Stars: 32450,
		},
		{
			Name:  "https://github.com/docker/metadata-action",
			Stars: 504,
		},
		{
			Name:  "https://github.com/docker/containerd-packaging",
			Stars: 47,
		},
		{
			Name:  "https://github.com/docker/docker-ce-packaging",
			Stars: 11111,
		},
		{
			Name:  "https://github.com/docker/index-cli-plugin",
			Stars: 36,
		},
		{
			Name:  "https://github.com/VKCOM/vk-api-schema",
			Stars: 192,
		},
		{
			Name:  "https://github.com/pytorch/pytorch",
			Stars: 61400,
		},
		{
			Name:  "https://github.com/LAION-AI/Open-Assistant",
			Stars: 2700,
		},
		{
			Name:  "https://github.com/2dust/v2rayN",
			Stars: 35300,
		},
		{
			Name:  "https://github.com/2dust/clashN",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/MasterGroosha/telegram-tutorial",
			Stars: 479,
		},
	}
	for _, item := range projects {
		m[item.Name] = item.Stars
	}
	for k, v := range m {
		fmt.Println(k, v)
	}
}
