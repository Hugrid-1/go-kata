// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	var buf bytes.Buffer // запишите данные в буфер
	for _, s := range data {
		n, err := buf.WriteString(s)
		check(err)
		if n != len(s) {
			fmt.Println("failed write to buffer")
			os.Exit(1)
		}
	}

	file, err := os.Create("example.txt") // создайте файл
	check(err)

	// запишите данные в файл
	_, err = io.Copy(file, &buf)
	check(err)
	file.Close()

	b, err := os.ReadFile("example.txt")
	check(err)
	_, err = buf.Write(b)
	check(err)
	data = []string{buf.String()}
	fmt.Println(data) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
