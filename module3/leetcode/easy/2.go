package easy

/*
	runtime 8 ms 81.87%
	memory 6.2 mb 99.42%
*/

func getConcatenation(nums []int) []int {
	doubleNums := make([]int, 0, 2*len(nums))
	copy(doubleNums, nums)
	copy(doubleNums[len(nums):], nums)
	return doubleNums
}
