package medium

func countPoints(points [][]int, queries [][]int) []int {
	resultPoints := make([]int, len(queries))
	for i, q := range queries {
		x, y, r := q[0], q[1], q[2]
		count := 0
		for _, p := range points {
			dx, dy := p[0]-x, p[1]-y
			if dx*dx+dy*dy <= r*r {
				count++
			}
		}
		resultPoints[i] = count
	}
	return resultPoints
}
