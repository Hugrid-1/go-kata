package main

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	//airConditioner = NewAirConditionerProxy(true) // with auth
	ChangeAuthorizationStatus(airConditioner, true)
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	ShowAuthJournal(airConditioner)
}
