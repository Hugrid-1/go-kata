package repository

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/models"
	"sync"
)

type Repository interface {
	Add(user models.User) error
	GetAll() []models.User
	GetByID(id int) (models.User, error)
	Delete(id int) (models.User, error)
}

type UserStorage struct {
	data               []*models.User
	primaryKeyIDx      map[int]*models.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:          make([]*models.User, 0),
		primaryKeyIDx: make(map[int]*models.User),
	}
}

func TestUserStorage() *UserStorage {
	testRepo := UserStorage{
		data:          make([]*models.User, 0),
		primaryKeyIDx: make(map[int]*models.User),
	}
	for i := 0; i < 100; i++ {
		testUser := models.User{
			ID:       testRepo.autoIncrementCount,
			Username: gofakeit.Username(),
			Email:    gofakeit.Email(),
			Phone:    gofakeit.Phone(),
		}
		err := testRepo.Add(testUser)
		if err != nil {
			panic("test repo creating crashed")
		}
	}
	return &testRepo
}

func (u *UserStorage) Add(user models.User) error {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[user.ID]; ok {
		return fmt.Errorf("User with  id %v already exist", user.ID)
	}
	user.ID = u.autoIncrementCount
	u.data = append(u.data, &user)
	u.primaryKeyIDx[user.ID] = &user
	u.autoIncrementCount++
	return nil
}

func (u *UserStorage) GetAll() []models.User {
	users := make([]models.User, 0)
	for _, user := range u.data {
		users = append(users, *user)
	}
	return users
}

func (u *UserStorage) GetByID(id int) (models.User, error) {
	if user, ok := u.primaryKeyIDx[id]; ok {
		return *user, nil
	} else {
		return models.User{}, fmt.Errorf("User not found")
	}
}

func (u *UserStorage) Delete(id int) (models.User, error) {
	u.Lock()
	defer u.Unlock()
	if user, ok := u.primaryKeyIDx[id]; ok {
		delete(u.primaryKeyIDx, id)
		for i, user := range u.data {
			if user.ID == id {
				u.data = append(u.data[:i], u.data[:i+1]...)
			}
		}
		return *user, nil
	}
	return models.User{}, fmt.Errorf("User not found")

}
