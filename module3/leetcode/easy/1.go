package easy

/*
	runtime 1 ms (72.4%)
	memory 1.8 MB (55.45%)
*/
func tribonacci(n int) int {
	var numArray = [3]int{0, 0, 1}
	result := 0
	if n < 3 {
		result = numArray[n]
		return result
	}
	for i := 3; i <= n; i++ {
		result = numArray[0] + numArray[1] + numArray[2]
		numArray[i%3] = result
	}
	return result
}
