package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	jsonStream := strings.NewReader(`
	{"Name":"Ross Geller","Age":28}
	{"Name":"Vladimir Zolkin","Age":20}
	{"Name":"Ivan Ivanov","Age":34}
	`)

	decoder := json.NewDecoder(jsonStream)

	var ross, monica Person

	_ = decoder.Decode(&ross)
	_ = decoder.Decode(&monica)

	fmt.Printf("ross: %#v\n", ross)
	fmt.Printf("monica: %#v\n", monica)

}
