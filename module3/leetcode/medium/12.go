package medium

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	n := list1
	var start, end *ListNode
	for i := 0; i <= b; i++ {
		if i == a-1 {
			start = n
		}
		if i == b {
			end = n
		}
		n = n.Next
	}
	start.Next = list2
	n = list2
	for n.Next != nil {
		n = n.Next
	}
	n.Next = end.Next
	return list1
}
