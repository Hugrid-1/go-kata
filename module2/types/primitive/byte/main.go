package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeByte()
}

func typeByte() {
	var b byte = 123
	fmt.Println("Size in bytes:", unsafe.Sizeof(b))
}
