package main

import (
	"testing"
	"time"
)

func generateTestData() *DoubleLinkedList {
	newList := &DoubleLinkedList{}
	err := newList.LoadData("data/doublelinkedlist_testBigData.json")
	if err != nil {
		panic(err)
	}
	return newList
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	newList := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := newList.LoadData("data/doublelinkedlist_testBigData.json")
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		newList.Prev()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		newList.Next()
	}
}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		newList.Len()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := newList.Index()
		if err != nil {
			return
		}
	}
}
func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		newList.Current()
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := newList.Reverse()
		if err != nil {
			return
		}
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := newList.Search("Kata secret commit")
		if err != nil {
			return
		}
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := newList.SearchUUID("6958b3ae-875b-11ed-8150-acde48001122")
		if err != nil {
			return
		}
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < newList.Len()-1; i++ {
		_, err := newList.Shift()
		if err != nil {
			return
		}
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < newList.Len()-1; i++ {
		_, err := newList.Pop()
		if err != nil {
			return
		}
	}
}
func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < newList.Len()-1; i++ {
		err := newList.Delete(1)
		if err != nil {
			return
		}
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	newList := generateTestData()
	b.ResetTimer()
	for i := 0; i < newList.Len()-1; i++ {
		err := newList.DeleteCurrent()
		if err != nil {
			return
		}
	}
}
func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	newList := generateTestData()
	testCommit := Commit{
		Message: "newTest commit",
		UUID:    "T3STUU1D--T3STUU1D--T3STUU1D-T3STUU1D--T3STUU1D-",
		Date:    time.Now(),
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := newList.Insert(0, testCommit)
		if err != nil {
			return
		}
	}
}
