package medium

import "sort"

/*
	runtime 72 ms (62.19%)
	memory 8.4 MB (34.15%)
*/

func sortTheStudents(score [][]int, k int) [][]int {
	var result [][]int
	studentsMap := make(map[int][]int, len(score))
	studentsKScoreArray := make([]int, len(score))
	for i, student := range score {
		studentsMap[student[k]] = student
		studentsKScoreArray[i] = student[k]
	}
	sort.Ints(studentsKScoreArray)
	for i := len(studentsKScoreArray) - 1; i >= 0; i-- {
		result = append(result, studentsMap[studentsKScoreArray[i]])
	}
	return result
}
