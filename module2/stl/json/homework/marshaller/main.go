package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

func (p Profile) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"f_count": %d}`, p.Followers)), nil
}

type Age int

func (a Age) MarshalText() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"age": %d}`, int(a))), nil
}

type Student struct {
	FirstName, lastName string
	Age                 Age
	Profile             Profile
}

func main() {
	john := &Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johnDoe91",
			Followers: 1975,
		},
	}

	johnJSON, _ := json.MarshalIndent(john, "", " ")
	fmt.Println(string(johnJSON))
}
