package main

import "testing"

var users = genUsers()
var products = genProducts()

func BenchmarkFirst(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSecond(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
