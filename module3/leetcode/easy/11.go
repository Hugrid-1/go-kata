package easy

/*
	runtime 0 ms (100%)
	memory 2.6 mb
*/

func runningSum(nums []int) []int {
	if len(nums) == 1 {
		return nums
	}
	for i := 1; i < len(nums); i++ {
		nums[i] = nums[i-1] + nums[i]
	}
	return nums
}
