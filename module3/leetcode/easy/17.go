package easy

import "strings"

/*
	runtime 0 ms 100%
	memory 3.5 mb 60.11%
*/

func mostWordsFound(sentences []string) int {
	mostWordsCount, wordsInSentence := 0, 0
	for _, sentence := range sentences {
		wordsInSentence = strings.Count(sentence, " ") + 1
		if wordsInSentence > mostWordsCount {
			mostWordsCount = wordsInSentence
		}
	}
	return mostWordsCount
}
