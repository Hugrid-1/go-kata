package medium

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	head := &TreeNode{}

	indexOfMaxValue := indexMaxValue(nums)
	head.Val = nums[indexOfMaxValue]
	head.Left = constructMaximumBinaryTree(nums[:indexOfMaxValue])
	head.Right = constructMaximumBinaryTree(nums[indexOfMaxValue+1:])

	return head

}

func indexMaxValue(nums []int) int {
	maxValue := 0
	maxValueIndex := 0
	for i := 0; i < len(nums); i++ {
		if nums[i] > maxValue {
			maxValue = nums[i]
			maxValueIndex = i
		}
	}
	return maxValueIndex
}
