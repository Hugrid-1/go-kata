package main

import (
	"fmt"
	"net"
)

func main() {

	// слушаем порт
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer listener.Close()
	for {
		//Принимаем вход соединение
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		go func() {
			defer conn.Close()
			data := make([]byte, 1024)
			n, err := conn.Read(data)
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println(string(data[:n]))
		}()
	}

}
