package repository

import (
	"fmt"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"sync"
)

type PetStorager interface {
	Create(pet models.Pet) models.Pet
	Update(pet models.Pet) (models.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (models.Pet, error)
	GetByStatus(status ...string) ([]models.Pet, error)
	AddPhotoUrl(petID int, filepath string) error
	GetList() []models.Pet
}

type PetStorage struct {
	data               []*models.Pet
	primaryKeyIDx      map[int]*models.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:          make([]*models.Pet, 0, 13),
		primaryKeyIDx: make(map[int]*models.Pet, 13),
	}
}

func (p *PetStorage) Create(pet models.Pet) models.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	return pet
}

func (p *PetStorage) GetByID(petID int) (models.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}
	return models.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[petID]; ok {
		delete(p.primaryKeyIDx, petID)
		for i, _ := range p.data {
			if p.data[i].ID == petID {
				p.data = append(p.data[:i], p.data[i+1:]...)
				return nil
			}
		}
	}

	return fmt.Errorf("Pet with ID %v not found in PetStorage.data", petID)
}

func (p *PetStorage) Update(pet models.Pet) (models.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		p.primaryKeyIDx[pet.ID] = &pet
		for i, _ := range p.data {
			if p.data[i].ID == pet.ID {
				p.data[i] = &pet
				return pet, nil
			}
		}
		return models.Pet{}, fmt.Errorf("Pet with ID %v not found in PetStorage.data", pet.ID)
	} else {
		return models.Pet{}, fmt.Errorf("Pet with ID %v  not found", pet.ID)
	}

}

func (p *PetStorage) GetList() []models.Pet {
	if len(p.data) == 0 {
		return []models.Pet{}
	}
	pets := make([]models.Pet, 0, len(p.data))
	for _, pet := range p.data {
		pets = append(pets, models.Pet{
			ID:        pet.ID,
			Category:  pet.Category,
			Name:      pet.Name,
			PhotoUrls: pet.PhotoUrls,
			Tags:      pet.Tags,
			Status:    pet.Status,
		})
	}
	return pets
}

func (p *PetStorage) GetByStatus(status ...string) ([]models.Pet, error) {
	filteredPets := make([]models.Pet, 0)
	found := false
	for i := range p.data {
		if contains(status, p.data[i].Status) {
			found = true
			filteredPets = append(filteredPets, *p.data[i])
		}
	}
	if found {
		return filteredPets, nil
	}
	return []models.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) AddPhotoUrl(petID int, filepath string) error {
	p.Lock()
	defer p.Unlock()
	if pet, ok := p.primaryKeyIDx[petID]; ok {
		pet.PhotoUrls = append(pet.PhotoUrls, filepath)
		return nil
	}
	return fmt.Errorf("not found")
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
