package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)
func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	if len(job.Dicts) < 2 {
		job.IsFinished = true
		return job, errNotEnoughDicts
	}
	job.Merged = make(map[string]string, len(job.Dicts))
	for _, dict := range job.Dicts {
		if dict == nil {
			job.IsFinished = true
			return job, errNilDict
		}
		for k, v := range dict {
			job.Merged[k] = v
		}
	}
	job.IsFinished = true
	return job, nil
}
func main() {
	result, err := ExecuteMergeDictsJob(&MergeDictsJob{})
	fmt.Println(result, err)

	result, err = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
	fmt.Println(result, err)

	result, err = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
	fmt.Println(result, err)

}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
