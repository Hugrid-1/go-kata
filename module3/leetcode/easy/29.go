package easy

func xorOperation(n int, start int) int {
	//array := make([]int,n)
	result := start
	for i := 1; i < n; i++ {
		result ^= start + 2*i
	}
	return result
}
