package repository

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"reflect"
	"testing"
)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

type TestUserRepository struct {
	File     io.ReadWriter
	FilePath string
	Users    []User
}

func CreateTestRepository(testUsers []User) *TestUserRepository {
	filePath := "./test.json"
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	check(err)
	err = file.Truncate(0)
	check(err)
	_, err = file.Seek(0, 0)
	check(err)
	testData, err := json.Marshal(testUsers)
	check(err)
	_, err = file.Write(testData)
	check(err)
	return &TestUserRepository{
		File:     file,
		FilePath: filePath,
		Users:    testUsers,
	}
}

func TestUserRepository_Find(t *testing.T) {
	testRepository := CreateTestRepository([]User{{1, "vova"}, {2, "maxim"}, {3, "alexey"}})
	type args struct {
		id int
	}

	tests := []struct {
		name       string
		repository *TestUserRepository
		args       args
		want       interface{}
		wantErr    bool
	}{
		{
			name:       "basic test",
			repository: testRepository,
			args:       args{1},
			want:       User{1, "vova"},
			wantErr:    false,
		},
		{
			name:       "negative test",
			repository: testRepository,
			args:       args{4},
			want:       User{},
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:     tt.repository.File,
				FilePath: tt.repository.FilePath,
				Users:    tt.repository.Users,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	testRepository := CreateTestRepository([]User{{1, "vova"}, {2, "maxim"}, {3, "alexey"}})

	tests := []struct {
		name       string
		repository *TestUserRepository
		want       []User
		wantErr    bool
	}{
		{
			name:       "basic test",
			repository: testRepository,
			want:       []User{{1, "vova"}, {2, "maxim"}, {3, "alexey"}},
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:     tt.repository.File,
				FilePath: tt.repository.FilePath,
				Users:    tt.repository.Users,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(len(got), len(tt.want)) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	testRepository := CreateTestRepository([]User{{1, "vova"}, {2, "maxim"}, {3, "alexey"}})
	type args struct {
		record interface{}
	}
	tests := []struct {
		name       string
		repository *TestUserRepository
		args       args
		wantErr    bool
		wantLen    int
	}{
		{
			name:       "basic test",
			repository: testRepository,
			args:       args{User{4, "vladimir"}},
			wantErr:    false,
			wantLen:    len(testRepository.Users) + 1,
		},
		{
			name:       "test with existing userID",
			repository: testRepository,
			args:       args{User{1, "vladimir"}},
			wantErr:    true,
			wantLen:    len(testRepository.Users),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:     tt.repository.File,
				FilePath: tt.repository.FilePath,
				Users:    tt.repository.Users,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantLen != len(r.Users) {
				t.Errorf("Save() want len = %v, but  len = %v", tt.wantLen, len(r.Users))
			}
		})
	}
}
