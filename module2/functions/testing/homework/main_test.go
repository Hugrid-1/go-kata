package main

import (
	"fmt"
	"testing"
	"unicode"
)

func Greet(name string) string {
	//Решение, при условии, что мы точно передаем в функцию либо английские символы, либо кириллицу.
	for _, s := range name {
		if unicode.Is(unicode.Cyrillic, s) {
			return fmt.Sprintf("Привет %s, добро пожаловать!", name)
		}
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}
