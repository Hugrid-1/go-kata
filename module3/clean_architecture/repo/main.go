package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/repo/repository"
)

func main() {
	filePath := "data/clean_architecture/test2.json"
	file, err := os.Create(filePath)

	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	user2 := repository.User{ID: 2, Name: "maxim"}
	user1 := repository.User{ID: 1, Name: "vova"}
	user3 := repository.User{ID: 3, Name: "alexey"}
	//user4 := repository.User{ID: 3, Name: "alexey"}
	r := repository.NewUserRepository(file, filePath)
	err = r.Save(user1)
	if err != nil {
		log.Fatalln(err)
	}
	err = r.Save(user2)
	if err != nil {
		log.Fatalln(err)
	}
	err = r.Save(user3)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(r.Find(2))
	fmt.Println(r.FindAll())
}
