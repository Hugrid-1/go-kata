package easy

/*
	runtime 8 ms 81.87%
	memory 6.2 mb 99.42%
*/

// !!! название содержит "2" т.к. задача повторяется с задачей 2 !!!
func getConcatenation2(nums []int) []int {
	doubleNums := make([]int, 0, 2*len(nums))
	copy(doubleNums, nums)
	copy(doubleNums[len(nums):], nums)
	return doubleNums
}
