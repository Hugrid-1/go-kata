package repository

import (
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"reflect"
	"sync"
	"testing"
)

type userFields struct {
	data                []*models.User
	primaryKeyIDx       map[int]*models.User
	primaryKeyUsernames map[string]*models.User
	autoIncrementCount  int
	Mutex               sync.Mutex
}

func getTestUserData() userFields {
	testFields := userFields{
		data: []*models.User{
			{ID: 0, Username: "testUsername1", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			{ID: 1, Username: "testUsername2", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			{ID: 2, Username: "testUsername3", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
		},
		primaryKeyIDx: map[int]*models.User{0: {ID: 0, Username: "testUsername1", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			1: {ID: 1, Username: "testUsername2", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			2: {ID: 2, Username: "testUsername3", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0}},
		primaryKeyUsernames: map[string]*models.User{"testUsername1": {ID: 0, Username: "testUsername1", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			"testUsername2": {ID: 1, Username: "testUsername2", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			"testUsername3": {ID: 2, Username: "testUsername3", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0}},
		autoIncrementCount: 3,
		Mutex:              sync.Mutex{},
	}
	return testFields
}

func TestUserStorage_Create(t *testing.T) {
	testData := getTestUserData()
	type args struct {
		user models.User
	}
	tests := []struct {
		name   string
		fields userFields
		args   args
		want   models.User
	}{
		{
			name:   "base test",
			fields: testData,
			args:   args{models.User{ID: 0, Username: "testUsername4", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0}},
			want:   models.User{ID: 3, Username: "testUsername4", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			if got := u.Create(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_CreateMultiple(t *testing.T) {
	testData := getTestUserData()
	type args struct {
		users []models.User
	}
	tests := []struct {
		name   string
		fields userFields
		args   args
		want   []models.User
	}{
		{
			name:   "base test",
			fields: testData,
			args: args{[]models.User{
				{ID: 0, Username: "testUsername4", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
				{ID: 0, Username: "testUsername5", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
				{ID: 0, Username: "testUsername6", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			}},
			want: []models.User{
				{ID: 3, Username: "testUsername4", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
				{ID: 4, Username: "testUsername5", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
				{ID: 5, Username: "testUsername6", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			if got := u.CreateMultiple(tt.args.users); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateMultiple() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	testData := getTestUserData()
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  userFields
		args    args
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{username: "testUsername1"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			if err := u.Delete(tt.args.username); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByName(t *testing.T) {
	testData := getTestUserData()
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  userFields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{username: "testUsername2"},
			want:    models.User{ID: 1, Username: "testUsername2", FirstName: "tst", LastName: "tst", Email: "tst", Password: "123", Phone: "123123", UserStatus: 0},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			got, err := u.GetByName(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_GetList(t *testing.T) {
	testData := getTestUserData()
	users := make([]models.User, 0)
	for _, user := range testData.data {
		users = append(users, *user)
	}
	tests := []struct {
		name   string
		fields userFields
		want   []models.User
	}{
		{
			name:   "base test",
			fields: testData,
			want:   users,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			if got := u.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	testData := getTestUserData()

	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		fields  userFields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{models.User{ID: 1, Username: "testUsername2", FirstName: "aaa", LastName: "aaa", Email: "aaa", Password: "123", Phone: "123123", UserStatus: 0}},
			want:    models.User{ID: 1, Username: "testUsername2", FirstName: "aaa", LastName: "aaa", Email: "aaa", Password: "123", Phone: "123123", UserStatus: 0},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage{
				data:                tt.fields.data,
				primaryKeyIDx:       tt.fields.primaryKeyIDx,
				primaryKeyUsernames: tt.fields.primaryKeyUsernames,
				autoIncrementCount:  tt.fields.autoIncrementCount,
				Mutex:               tt.fields.Mutex,
			}
			got, err := u.Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
