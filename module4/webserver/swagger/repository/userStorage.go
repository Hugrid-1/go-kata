package repository

import (
	"fmt"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"sync"
)

type UserStorager interface {
	Create(user models.User) models.User
	CreateMultiple(users []models.User) []models.User
	Update(user models.User) (models.User, error)
	Delete(username string) error
	GetByName(username string) (models.User, error)
	GetList() []models.User
}
type UserStorage struct {
	data                []*models.User
	primaryKeyIDx       map[int]*models.User
	primaryKeyUsernames map[string]*models.User
	autoIncrementCount  int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:                make([]*models.User, 0, 13),
		primaryKeyIDx:       make(map[int]*models.User, 13),
		primaryKeyUsernames: make(map[string]*models.User, 13),
	}
}

func (u *UserStorage) Create(user models.User) models.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.ID] = &user
	u.primaryKeyUsernames[user.Username] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)
	return user
}

func (u *UserStorage) CreateMultiple(users []models.User) []models.User {
	for i, user := range users {
		users[i] = u.Create(user)
	}
	return users
}

func (u *UserStorage) Update(user models.User) (models.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[user.ID]; ok {
		u.primaryKeyIDx[user.ID] = &user
		u.primaryKeyUsernames[user.Username] = &user
		for i, _ := range u.data {
			if u.data[i].ID == user.ID {
				u.data[i] = &user
				return user, nil
			}
		}
	}

	return models.User{}, fmt.Errorf("not found")
}

func (u *UserStorage) Delete(username string) error {
	u.Lock()
	defer u.Unlock()
	if user, ok := u.primaryKeyUsernames[username]; ok {
		delete(u.primaryKeyIDx, user.ID)
		delete(u.primaryKeyUsernames, username)
		for i, _ := range u.data {
			if u.data[i].Username == username {
				u.data = append(u.data[:i], u.data[i+1:]...)
				return nil
			}
		}
		return nil
	}
	return fmt.Errorf("not found")
}

func (u *UserStorage) GetByName(username string) (models.User, error) {
	if user, ok := u.primaryKeyUsernames[username]; ok {
		return *user, nil
	}
	return models.User{}, fmt.Errorf("not found")
}

func (u *UserStorage) GetList() []models.User {
	users := make([]models.User, 0)
	for _, user := range u.data {
		users = append(users, *user)
	}
	return users
}
