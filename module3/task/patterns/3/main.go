package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// WeatherAPI is the commandInterface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

type MainWeatherData struct {
	Temperature          float64 `json:"temp"`
	TemperatureFeelsLike float64 `json:"feels_like"`
	Pressure             float64 `json:"pressure"`
	Humidity             float64 `json:"humidity"`
}
type WindData struct {
	Speed float64 `json:"speed"`
	Deg   float64 `json:"deg"`
	Gust  float64 `json:"gust"`
}

type WeatherData struct {
	MainWeatherData `json:"main"`
	WindData        `json:"wind"`
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	weatherData, err := o.getWeatherData(location, "RU", 5)
	if err != nil {
		log.Fatalln(err)
	}
	temperature := int(weatherData.MainWeatherData.Temperature)
	return temperature
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	weatherData, err := o.getWeatherData(location, "RU", 5)
	if err != nil {
		log.Fatalln(err)
	}
	humidity := int(weatherData.MainWeatherData.Humidity)
	return humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	weatherData, err := o.getWeatherData(location, "RU", 5)
	if err != nil {
		log.Fatalln(err)
	}

	windSpeed := int(weatherData.WindData.Speed)
	return windSpeed
}

func (o *OpenWeatherAPI) getWeatherData(location string, regionCode string, limit int) (WeatherData, error) {
	weatherData := WeatherData{}
	resp, err := http.Get(fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?q=%v,%v&limit=%v&appid=%v", location, regionCode, limit, o.apiKey))
	if err != nil {
		return weatherData, err
	}
	defer resp.Body.Close()
	requestBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return weatherData, err
	}

	err = json.Unmarshal(requestBody, &weatherData)

	return weatherData, err
}

// WeatherFacade is the facade that provides a simplified commandInterface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("7eaae7fc27662b7801465e5bf9ce2692")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}
