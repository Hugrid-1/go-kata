package easy

func countDigits(num int) int {
	resultCounter := 0
	for n := num; n > 0; {
		if num%(n%10) == 0 {
			resultCounter++
		}
		n /= 10
	}
	return resultCounter
}
