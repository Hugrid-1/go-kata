package repository

type Repository struct {
	PetStorager
	UserStorager
	StoreStorager
}

func NewRepository() *Repository {
	return &Repository{
		PetStorager:   NewPetStorage(),
		UserStorager:  NewUserStorage(),
		StoreStorager: NewStoreStorage(),
	}
}
