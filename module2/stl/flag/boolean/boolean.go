package main

import (
	"flag"
	"fmt"
)

type Color string

const (
	ColorBlack  Color = "\u001b[30m"
	ColorRed    Color = "\u001b[31m"
	ColorGreen  Color = "\u001B[32m"
	ColorYellow Color = "\u001B[33m"
	ColorBlue   Color = "\u001B[34m"
	ColorReset  Color = "\u001B[0m"
)

func colorize(color Color, message string) {
	fmt.Println(string(color), message, string(ColorReset))
}

func main() {
	useColor := flag.Bool("color", false, "display colorized output")
	flag.Parse()

	if *useColor {
		colorize(ColorBlue, "Hello, Kata Academy")
		return
	}
	fmt.Println("Hello, Kata Academy")
}
