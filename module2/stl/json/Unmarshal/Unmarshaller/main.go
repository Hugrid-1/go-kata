package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Profile struct {
	Username  string
	Followers string
}

func (p *Profile) UnmarshalJSON(data []byte) error {

	//unmarshall
	var container map[string]interface{}
	_ = json.Unmarshal(data, &container)
	fmt.Printf("container: %T / %#v\n\n", container, container)

	//extract commandInterface value
	iUserName := container["Username"]
	iFollowers := container["f_count"]
	fmt.Printf("iuserName: %T/%#v\n", iUserName, iUserName)
	fmt.Printf("iFollowers: %T/%#v\n", iFollowers, iFollowers)

	userName := iUserName.(string)
	followers := iFollowers.(float64)
	fmt.Printf("userName: %T/%#v\n", userName, userName)
	fmt.Printf("followers: %T/%#v\n", followers, followers)

	p.Username = strings.ToUpper(userName)
	p.Followers = fmt.Sprintf("%.2fk", followers/1000)

	return nil
}

type Student struct {
	FirstName string
	Profile   Profile
}

func main() {
	data := []byte(`
	{
		"FirstName": "John",
		"Profile": {
			"Username": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john Student

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n", john)
}
