package medium

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	var unreachable = make(map[int]struct{})
	for i := 0; i < n; i++ {
		unreachable[i] = struct{}{}
	}

	for _, edge := range edges {
		delete(unreachable, edge[1])
	}

	var result []int
	for k := range unreachable {
		result = append(result, k)
	}
	return result
}
