package medium

func groupThePeople(groupSizes []int) [][]int {
	m := make(map[int][]int)
	for i, size := range groupSizes {
		m[size] = append(m[size], i)
	}

	var resultGroups [][]int
	for size, group := range m {
		n := len(group)
		for n > 0 {
			resultGroups = append(resultGroups, group[n-size:n])
			n -= size
		}
	}

	return resultGroups
}
