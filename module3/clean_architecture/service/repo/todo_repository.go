package repo

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/service/model"
)

// Repository layer
var noTaskError = fmt.Errorf("Task not found")

// Task represents a to-do ta

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) (model.Todo, error)
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
	SortTasks() error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewFIleTaskRepository(path string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: path}
}

// GetTasks returns all tasks from the repository
func (r *FileTaskRepository) GetTasks() ([]model.Todo, error) {
	var tasks []model.Todo

	file, err := os.Open(r.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}
	if len(content) != 0 {
		if err := json.Unmarshal(content, &tasks); err != nil {
			return nil, err
		}
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (r *FileTaskRepository) GetTask(id int) (model.Todo, error) {
	var task model.Todo

	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == id {
			return t, nil
		}
		if i == len(tasks)-1 {
			return task, noTaskError
		}
	}

	return task, nil
}

// CreateTask adds a new task to the repository
func (r *FileTaskRepository) CreateTask(task model.Todo) (model.Todo, error) {
	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := r.saveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (r *FileTaskRepository) UpdateTask(task model.Todo) (model.Todo, error) {
	tasks, err := r.GetTasks()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
		if i == len(tasks)-1 {
			return task, noTaskError
		}
	}

	if err := r.saveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (r *FileTaskRepository) DeleteTask(id int) error {
	file, err := os.OpenFile(r.FilePath, os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	tasks, err := r.GetTasks()
	if err != nil {
		return err
	}

	for i, task := range tasks {
		if task.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
		if i == len(tasks)-1 {
			return noTaskError
		}
	}
	data, err := json.Marshal(tasks)
	if err != nil {
		return err
	}

	err = file.Truncate(0)
	if err != nil {
		return err
	}
	_, err = file.Seek(0, 0)
	if err != nil {
		return err
	}

	_, err = file.Write(data)

	if err != nil {
		return err
	}

	return err
}

func (r *FileTaskRepository) SortTasks() ([]model.Todo, error) {
	tasks, err := r.GetTasks()
	if err != nil {
		return tasks, err
	}
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(tasks)-i; j++ {
			if tasks[j].Status > tasks[j+1].Status {
				tasks[j], tasks[j+1] = tasks[j+1], tasks[j]
				swapped = true
			}
		}
		i++
	}
	if err := r.saveTasks(tasks); err != nil {
		return tasks, err
	}
	return tasks, err
}

func (r *FileTaskRepository) saveTasks(tasks []model.Todo) error {
	file, err := os.OpenFile(r.FilePath, os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := json.Marshal(tasks)
	if err != nil {
		return err
	}
	err = file.Truncate(0)
	if err != nil {
		return err
	}
	_, err = file.Seek(0, 0)
	if err != nil {
		return err
	}
	_, err = file.Write(data)

	if err != nil {
		return err
	}
	return err
}
