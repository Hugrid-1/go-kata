package easy

/*
	runtime 0 ms 100%
	memory 1.9 mb 100%
*/

func sum(num1 int, num2 int) int {
	return num1 + num2
}
