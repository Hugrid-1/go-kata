package medium

func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	var stack []*TreeNode
	curr := root
	sum := 0
	for len(stack) != 0 || curr != nil {
		for curr != nil {
			stack = append(stack, curr)
			curr = curr.Right
		}
		curr, stack = stack[len(stack)-1], stack[:len(stack)-1]
		sum += curr.Val
		curr.Val = sum
		curr = curr.Left
	}
	return root
}
