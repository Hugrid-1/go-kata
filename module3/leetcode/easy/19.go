package easy

import "sort"

/*
	runtime 1 ms 75%
	memory 1.8 mb 100%
*/

func minimumSum(num int) int {
	digits := make([]int, 4)
	digits[0] = num / 1000
	digits[1] = (num % 1000) / 100
	digits[2] = (num % 100) / 10
	digits[3] = num % 10
	sort.Ints(digits)

	if digits[0] == 0 {
		return digits[1]*10 + digits[2] + digits[3]
	} else if digits[0] == 0 && digits[1] == 0 {
		return digits[2] + digits[3]
	}

	return digits[0]*10 + digits[1]*10 + digits[2] + digits[3]

}
