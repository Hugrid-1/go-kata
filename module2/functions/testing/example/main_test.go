package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAdd(t *testing.T) {
	type testCase struct {
		a        int
		b        int
		expected int
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 4},
		{a: 1, b: 4, expected: 5},
		{a: 3, b: 7, expected: 10},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		recievedResult := Add(test.a, test.b)
		assert.Equal(t, expectedResult, recievedResult)

		fmt.Printf("case : a = %d, b = %d while expected result equals to %d, recieved result equals to %d\n", test.a, test.b, test.expected, recievedResult)
	}
}

func TestAddWithTableDrivenTesting(t *testing.T) {
	type args struct {
		a int
		b int
	}

	type testCase struct {
		name string
		args args
		want int
	}

	testCases := []testCase{
		{
			name: "a and b equal",
			args: args{a: 2, b: 2},
			want: 4,
		},
		{
			name: "a is less than b",
			args: args{a: 1, b: 5},
			want: 6,
		},
		{
			name: "a is bigger than b",
			args: args{a: 5, b: 3},
			want: 8,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.want, Add(test.args.a, test.args.b), "Add(%v,%v)", test.args.a, test.args.b)
		})
	}
}

func TestMultiply(t *testing.T) {
	type testCase struct {
		a        int
		b        int
		expected int
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 4},
		{a: 1, b: 4, expected: 4},
		{a: 3, b: 7, expected: 21},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		recievedResult := Multiply(test.a, test.b)
		assert.Equal(t, expectedResult, recievedResult)

		fmt.Printf("case : a = %d, b = %d while expected result equals to %d, recieved result equals to %d\n", test.a, test.b, test.expected, recievedResult)
	}
}
