package commandInterface

import (
	"fmt"
	"strconv"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/cli/model"
)

var mainMenu = "------MAIN MENU------;\n" + "a. Показать список задач;\n" +
	"b. Добавить задачу;\n" +
	"c. Удалить задачу;\n" +
	"d. Редактировать задачу по ID;\n" +
	"f. Завершить задачу;\n" +
	"e. Exit."
var noTaskError = fmt.Errorf("Task not found")

type ToDoService interface {
	ListTodos() ([]model.Todo, error)
	GetTodo(id int) (model.Todo, error)
	CreateTodo(title string) error
	EditTodo(todo model.Todo, newTitle string, newDescription string, newStatus string) error
	RemoveTodo(todo model.Todo) error
}

type cli struct {
	service       ToDoService
	processStatus bool
}

func NewCli(service ToDoService) *cli {
	return &cli{
		service:       service,
		processStatus: true,
	}
}

func (c *cli) GetStatus() bool {
	return c.processStatus
}
func (c *cli) StartWork() {
	fmt.Println("Привет!\n Для взаимодействия с пунктами меню ввведи букву соответствующего пункта.\n Если тебе необходимо закончить работу введи 'e' ")
	c.ShowMainMenu()
}

func (c *cli) ShowMainMenu() {
	fmt.Println(mainMenu)
	var input string
	_, err := fmt.Scanln(&input)
	if err != nil {
		c.restartCliWithError(err)
		return
	}
	if len(input) > 1 {
		c.restartCliWithError(fmt.Errorf("Ошибка. Введено более 1 символа"))
		return
	}
	switch input {
	case "a":
		c.PrintToDoList()
	case "b":
		c.ShowAddMenu()
	case "c":
		c.ShowDeleteTaskMenu()
	case "d":
		c.ShowEditMenu()
	case "f":
		c.ShowFinishTaskMenu()
	case "e":
		c.FinishCli()
	default:
		c.restartCliWithError(fmt.Errorf("Ошибка. Такого символа нет в меню."))

	}
	fmt.Println()

}
func (c *cli) PrintToDoList() {
	todoList, err := c.service.ListTodos()
	if err != nil {
		c.restartCliWithError(err)
	}
	fmt.Printf("Всего задач: %v\n", len(todoList))
	for _, todo := range todoList {
		fmt.Printf("ID: %v\n Title: %v\n Description: %v\n Status: %v \n", todo.ID, todo.Title, todo.Description, todo.Status)
	}
	c.ShowMainMenu()
}

func (c *cli) ShowAddMenu() {
	fmt.Println("Введи название новой задачи:")
	var input string
	_, err := fmt.Scanln(&input)
	if err != nil {
		c.restartCliWithError(err)
		return
	}
	err = c.service.CreateTodo(input)
	if err != nil {
		c.restartCliWithError(err)
		return
	}
	fmt.Println("Задача добавлена!")
	c.ShowMainMenu()
}

func (c *cli) ShowDeleteTaskMenu() {
	todo, err := c.getTodoProcess()
	if err != nil {
		if err == noTaskError {
			fmt.Println(err)
			c.ShowMainMenu()
			return
		} else {
			c.restartCliWithError(err)
			return
		}
	}
	err = c.service.RemoveTodo(todo)
	if err != nil {
		c.restartCliWithError(err)
	} else {
		fmt.Println("Задача удалена!")
		c.ShowMainMenu()
	}

}
func (c *cli) ShowEditMenu() {
	todo, err := c.getTodoProcess()
	if err != nil {
		if err == noTaskError {
			fmt.Println(err)
		} else {
			c.restartCliWithError(err)
		}
	}

	fmt.Println("Введи новое название задачи:")
	_, err = fmt.Scanln(&todo.Title)
	if err != nil {
		c.restartCliWithError(err)
		return
	}
	fmt.Println("Введи новое описание задачи:")
	_, err = fmt.Scanln(&todo.Description)
	if err != nil {
		c.restartCliWithError(err)
		return
	}

	err = c.service.EditTodo(todo, todo.Title, todo.Description, todo.Status)
	if err != nil {
		c.restartCliWithError(err)
	} else {
		fmt.Println("Задача изменена!")
		c.ShowMainMenu()
	}
}
func (c *cli) ShowFinishTaskMenu() {
	todo, err := c.getTodoProcess()
	if err != nil {
		if err == noTaskError {
			fmt.Println(err)
		} else {
			c.restartCliWithError(err)
		}
	}
	err = c.service.EditTodo(todo, todo.Title, todo.Description, "finished")
	if err != nil {
		c.restartCliWithError(err)

	} else {
		fmt.Println("Изменен статус задачи!")
		c.ShowMainMenu()
	}
}
func (c *cli) FinishCli() {
	fmt.Println("Работа завершена! Пока")
	c.processStatus = false
}

func (c *cli) restartCliWithError(err error) {
	fmt.Printf("CLI RESTART WORK WITH ERROR: %v\n", err)
	c.ShowMainMenu()
}

func (c *cli) getTodoProcess() (model.Todo, error) {
	fmt.Println("Введи ID задачи:")
	var input string
	_, err := fmt.Scanln(&input)
	if err != nil {
		return model.Todo{}, err
	}
	id, err := strconv.Atoi(input)
	if err != nil {
		return model.Todo{}, err
	}
	todo, err := c.service.GetTodo(id)
	if err != nil {
		return model.Todo{}, err
	}
	return todo, err
}
