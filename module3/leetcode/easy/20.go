package easy

func kidsWithCandies(candies []int, extraCandies int) []bool {
	result := make([]bool, len(candies))
	maxCandies := 0
	for _, candie := range candies {
		if candie > maxCandies {
			maxCandies = candie
		}
	}
	for i := range candies {
		if candies[i]+extraCandies >= maxCandies {
			result[i] = true
		} else {
			result[i] = false
		}
	}
	return result
}
