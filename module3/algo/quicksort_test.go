package algo

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func randomData(n, max int) func() []int {
	var data []int
	return func() []int {
		if data != nil {
			return data
		}
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
		return data
	}
}

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}
	return data
}

func TestQuickSort(t *testing.T) {
	data := randomData(100, 1000)
	sorted := append([]int{}, data()...)
	sort.Ints(sorted)
	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed slice",
			args: args{
				arr: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1},
			},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
		{
			name: "random 1000",
			args: args{
				arr: data(),
			},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := QuickSort(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QuickSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkQuickSort(b *testing.B) {
	data := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data[i])
	}
}
