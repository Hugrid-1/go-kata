package medium

func pairSum(head *ListNode) int {
	currentNode := head
	arr := make([]int, 0)
	maxSum := 0

	for currentNode != nil {
		arr = append(arr, currentNode.Val)
		currentNode = currentNode.Next
	}

	sumOfNodes := 0
	for i := 0; i < len(arr)/2; i++ {
		sumOfNodes = arr[i] + arr[len(arr)-i-1]
		if sumOfNodes > maxSum {
			maxSum = sumOfNodes
		}
	}

	return maxSum
}
