package easy

func createTargetArray(nums []int, index []int) []int {
	targetArray := make([]int, 0, len(nums))
	for i := range nums {
		targetArray = insert(targetArray, nums[i], index[i])
	}
	return targetArray
}

func insert(array []int, element int, i int) []int {
	return append(array[:i], append([]int{element}, array[i:]...)...)
}
