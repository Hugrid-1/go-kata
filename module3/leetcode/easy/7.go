package easy

import "strings"

/*
	runtime 1 ms 68.40 %
	memory 1.8 mb 65.80 %
*/

func defangIPaddr(address string) string {
	return strings.Replace(address, ".", "[.]", -1)
}
