package main

import "fmt"

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Adress string
	City   string
	index  string
}

// Base Embedding
type Base struct {
	b int
}

type Container struct {
	Base
	c string
}

//

func main() {
	user := User{
		Age:  20,
		Name: "Vladimir",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Adress: "Novovatutinskaya 3 street,13,2",
			City:   "Moscow",
			index:  "108836",
		},
	}
	fmt.Println(user2)

	container := Container{
		Base: Base{b: 55},
		c:    "Test field",
	}
	fmt.Println(container.Base.b)
	fmt.Println(container.b)
}
