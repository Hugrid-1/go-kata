package easy

/*
	runtime 9 ms 90.24%
	memory 5.3 mb 22.49%
*/

func differenceOfSum(nums []int) int {
	var result int
	for _, num := range nums {
		result += num
		result -= num/1000 + (num%1000)/100 + (num%100)/10 + num%10

	}
	return result
}
