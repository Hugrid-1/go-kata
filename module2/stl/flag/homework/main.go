package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	conf := Config{}
	var filename string

	flag.StringVar(&filename, "conf", "", "to read config file. need enter filename")
	flag.Parse()

	if filename != "" {
		f, err := os.Open(filename)
		if err != nil {
			fmt.Println("error opening file: err:", err)
			os.Exit(1)
		}
		defer f.Close()
		decoder := json.NewDecoder(f)
		err = decoder.Decode(&conf)
		if err != nil {
			fmt.Println("failed decode file: err:", err)
			os.Exit(1)
		}
		fmt.Printf("Production:%v\nAppName: %v\n", conf.Production, conf.AppName)

	} else {
		fmt.Println("You must pass a filename of json config")
		os.Exit(1)
	}
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
