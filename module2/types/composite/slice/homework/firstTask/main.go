package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = AppendReturn(s)
	fmt.Println(s)

	AppendByLink(&s)
	fmt.Println(s)

}

// first variant
func AppendReturn(s []int) []int {
	s = append(s, 4)
	return s
}

// second variant
func AppendByLink(s *[]int) {
	*s = append(*s, 5)
}
