package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Student struct {
	FirstName, LastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"LastName": "Doe",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English", "Spanish", "German" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": { "Math": "A" },
		"Profile": {
			"Username": "johndoe91",
			"Followers": 1975
		}
	}`)

	var john Student = Student{
		IsMale:   true,
		Subjects: []string{"Art"},
		Grades:   map[string]string{"Science": "A+"},
	}

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n", john)
}

/*
func validJson() {
	fmt.Println("-------------------------------------------------")
	fmt.Println("Test  Valid JSON")
	data := []byte(`
	{
		"FirstName": "John",
		"Age": 21,
		"Username": "johndoe91",
		"Grades": null,
		"Languages": [
		   "English",
		   "French"
		]
	}`)

	isValid := json.Valid(data)
	fmt.Println(string(data), isValid)
	fmt.Println("-------------------------------------------------")
}
*/
