package easy

/*
	runtime 13 ms 88.83%
	memory 6.4 mb 97.82%
*/
func buildArray(nums []int) []int {
	resultArray := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		resultArray[i] = nums[nums[i]]
	}
	return resultArray
}
