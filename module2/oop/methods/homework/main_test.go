package main

import "testing"

func TestDivide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "first",
			args: args{a: 2, b: 2},
			want: 1,
		},
		{
			name: "second",
			args: args{a: 3, b: 2},
			want: 1.5,
		},
		{
			name: "third",
			args: args{a: 4, b: 2},
			want: 2,
		},
		{
			name: "fourth",
			args: args{a: 100, b: 4},
			want: 25,
		},
		{
			name: "fifth",
			args: args{a: 250, b: 2.5},
			want: 100,
		},
		{
			name: "sixth",
			args: args{a: 100, b: 8},
			want: 12.5,
		},
		{
			name: "seventh",
			args: args{a: 10.24, b: 2.56},
			want: 4,
		},
		{
			name: "eighth",
			args: args{a: 5, b: 4},
			want: 1.25,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Divide() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMultiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "first",
			args: args{a: 2, b: 2},
			want: 4,
		},
		{
			name: "second",
			args: args{a: 3, b: 2},
			want: 6,
		},
		{
			name: "third",
			args: args{a: 4, b: 2},
			want: 8,
		},
		{
			name: "fourth",
			args: args{a: 100, b: 4},
			want: 400,
		},
		{
			name: "fifth",
			args: args{a: 250, b: 2.5},
			want: 625,
		},
		{
			name: "sixth",
			args: args{a: 10, b: 8.25},
			want: 82.5,
		},
		{
			name: "seventh",
			args: args{a: 10.24, b: 2.56},
			want: 26.2144,
		},
		{
			name: "eighth",
			args: args{a: 5.21, b: 492.1},
			want: 2563.841,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Multuply() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAverage(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "first",
			args: args{a: 2, b: 2},
			want: 2,
		},
		{
			name: "second",
			args: args{a: 3, b: 2},
			want: 2.5,
		},
		{
			name: "third",
			args: args{a: 4, b: 2},
			want: 3,
		},
		{
			name: "fourth",
			args: args{a: 100, b: 4},
			want: 52,
		},
		{
			name: "fifth",
			args: args{a: 250, b: 2.5},
			want: 126.25,
		},
		{
			name: "sixth",
			args: args{a: 17, b: 4},
			want: 10.5,
		},
		{
			name: "seventh",
			args: args{a: 10.24, b: 2.56},
			want: 6.4,
		},
		{
			name: "eighth",
			args: args{a: 5, b: 4},
			want: 4.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Average() = %v, want %v", got, tt.want)
			}
		})
	}
}
