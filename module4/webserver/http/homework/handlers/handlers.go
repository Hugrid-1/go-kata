package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/models"
	"net/http"
	"strconv"
)

type UserService interface {
	ListUsers() []models.User
	GetUser(id int) (models.User, error)
}

type UserHandler struct { // Pet контроллер
	service UserService
}

func NewUserHandler(service UserService) *UserHandler { // конструктор нашего контроллера
	return &UserHandler{service: service}
}

func (u *UserHandler) GetUserByID(w http.ResponseWriter, r *http.Request) {

	idRaw := chi.URLParam(r, "id") // получаем petID из chi router
	id, err := strconv.Atoi(idRaw)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		fmt.Errorf("Conver error : %v - > integer", idRaw)
		return
	}
	user, err := u.service.GetUser(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (u *UserHandler) GetAllUsers(w http.ResponseWriter, r *http.Request) {

	user := u.service.ListUsers()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
