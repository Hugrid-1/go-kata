package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r := r.(type) {
	case *int:
		if r == nil {
			fmt.Println("Success!")
		}
	}

}
