package easy

/*
	runtime 6 ms 55.48%
	memory 3.6 mb 92.41%
*/

func shuffle(nums []int, n int) []int {
	if n == 1 {
		return nums
	}
	resultArray := make([]int, 2*n)
	for i, j := 0, 0; j < n; {
		resultArray[i], resultArray[i+1] = nums[j], nums[j+n]
		i += 2
		j++
	}
	resultArray[2*n-1], resultArray[2*n-2] = nums[2*n-1], nums[n-1]
	return resultArray
}
