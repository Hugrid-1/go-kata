package easy

func smallerNumbersThanCurrent(nums []int) []int {
	result := make([]int, len(nums))
	for i := range result {
		for j := range nums {
			if nums[j] < nums[i] {
				result[i]++
			}
		}
	}
	return result
}
