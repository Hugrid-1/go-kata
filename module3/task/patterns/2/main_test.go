package main

import (
	"reflect"
	"testing"
	"time"
)

func TestAirConditionerAdapter_SetTemperature(t *testing.T) {
	type fields struct {
		realAirConditioner *RealAirConditioner
	}
	type args struct {
		temp int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := AirConditionerAdapter{
				realAirConditioner: tt.fields.realAirConditioner,
			}
			rc.SetTemperature(tt.args.temp)
		})
	}
}

func TestAirConditionerAdapter_TurnOff(t *testing.T) {
	type fields struct {
		realAirConditioner *RealAirConditioner
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := AirConditionerAdapter{
				realAirConditioner: tt.fields.realAirConditioner,
			}
			rc.TurnOff()
		})
	}
}

func TestAirConditionerAdapter_TurnOn(t *testing.T) {
	type fields struct {
		realAirConditioner *RealAirConditioner
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := AirConditionerAdapter{
				realAirConditioner: tt.fields.realAirConditioner,
			}
			rc.TurnOn()
		})
	}
}

func TestAirConditionerProxy_SetTemperature(t *testing.T) {
	type fields struct {
		airConditionerAdapter *AirConditionerAdapter
		authStatus            bool
		logJournal            map[time.Time]string
	}
	type args struct {
		temp int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := &AirConditionerProxy{
				airConditionerAdapter: tt.fields.airConditionerAdapter,
				authStatus:            tt.fields.authStatus,
				logJournal:            tt.fields.logJournal,
			}
			rc.SetTemperature(tt.args.temp)
		})
	}
}

func TestAirConditionerProxy_TurnOff(t *testing.T) {
	type fields struct {
		airConditionerAdapter *AirConditionerAdapter
		authStatus            bool
		logJournal            map[time.Time]string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := &AirConditionerProxy{
				airConditionerAdapter: tt.fields.airConditionerAdapter,
				authStatus:            tt.fields.authStatus,
				logJournal:            tt.fields.logJournal,
			}
			rc.TurnOff()
		})
	}
}

func TestAirConditionerProxy_TurnOn(t *testing.T) {
	type fields struct {
		airConditionerAdapter *AirConditionerAdapter
		authStatus            bool
		logJournal            map[time.Time]string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := &AirConditionerProxy{
				airConditionerAdapter: tt.fields.airConditionerAdapter,
				authStatus:            tt.fields.authStatus,
				logJournal:            tt.fields.logJournal,
			}
			rc.TurnOn()
		})
	}
}

func TestChangeAuthorizationStatus(t *testing.T) {
	type args struct {
		proxy  *AirConditionerProxy
		status bool
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ChangeAuthorizationStatus(tt.args.proxy, tt.args.status)
		})
	}
}

func TestNewAirConditionerProxy(t *testing.T) {
	type args struct {
		authStatus bool
	}
	tests := []struct {
		name string
		args args
		want *AirConditionerProxy
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAirConditionerProxy(tt.args.authStatus); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAirConditionerProxy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRealAirConditioner_SetTemperature(t *testing.T) {
	type fields struct {
		temperature int
	}
	type args struct {
		temp int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := RealAirConditioner{
				temperature: tt.fields.temperature,
			}
			rc.SetTemperature(tt.args.temp)
		})
	}
}

func TestRealAirConditioner_TurnOff(t *testing.T) {
	type fields struct {
		temperature int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := RealAirConditioner{
				temperature: tt.fields.temperature,
			}
			rc.TurnOff()
		})
	}
}

func TestRealAirConditioner_TurnOn(t *testing.T) {
	type fields struct {
		temperature int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rc := RealAirConditioner{
				temperature: tt.fields.temperature,
			}
			rc.TurnOn()
		})
	}
}

func TestShowAuthJournal(t *testing.T) {
	type args struct {
		proxy *AirConditionerProxy
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ShowAuthJournal(tt.args.proxy)
		})
	}
}

func Test_remoteControl(t *testing.T) {
	type args struct {
		button         string
		airConditioner *RealAirConditioner
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			remoteControl(tt.args.button, tt.args.airConditioner)
		})
	}
}

func Test_remoteSet(t *testing.T) {
	type args struct {
		button         string
		value          int
		airConditioner *RealAirConditioner
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			remoteSet(tt.args.button, tt.args.value, tt.args.airConditioner)
		})
	}
}
