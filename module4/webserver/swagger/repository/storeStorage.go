package repository

import (
	"fmt"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"sync"
)

type StoreStorager interface {
	Create(order models.Store) models.Store
	Delete(orderID int) error
	GetByID(orderID int) (models.Store, error)
	GetStatusCountMap() map[string]int
}

type StoreStorage struct {
	data               []*models.Store
	primaryKeyIDx      map[int]*models.Store
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		data:          make([]*models.Store, 0, 13),
		primaryKeyIDx: make(map[int]*models.Store, 13),
	}
}

func (s *StoreStorage) Create(order models.Store) models.Store {
	s.Lock()
	defer s.Unlock()
	order.ID = s.autoIncrementCount
	s.primaryKeyIDx[order.ID] = &order
	s.autoIncrementCount++
	s.data = append(s.data, &order)
	return order
}

func (s *StoreStorage) GetByID(orderID int) (models.Store, error) {
	if v, ok := s.primaryKeyIDx[orderID]; ok {
		return *v, nil
	}
	return models.Store{}, fmt.Errorf("not found")
}

func (s *StoreStorage) Delete(orderID int) error {
	s.Lock()
	defer s.Unlock()
	if _, ok := s.primaryKeyIDx[orderID]; ok {
		delete(s.primaryKeyIDx, orderID)
		for i, _ := range s.data {
			if s.data[i].ID == orderID {
				s.data = append(s.data[:i], s.data[i+1:]...)
				return nil
			}
		}
	}

	return fmt.Errorf("not found")
}
func (s *StoreStorage) GetStatusCountMap() map[string]int {
	countMap := make(map[string]int)
	for _, order := range s.data {
		if _, ok := countMap[order.Status]; ok {
			countMap[order.Status]++
		} else {
			countMap[order.Status] = 1
		}
	}
	return countMap
}
