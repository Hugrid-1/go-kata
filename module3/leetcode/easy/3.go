package easy

/*
	runtime 1 ms 65%
	memory 1.8 mb 63.33 %
*/
func convertTemperature(celsius float64) []float64 {
	return []float64{celsius + 273.15, celsius*1.80 + 32.00}
}
