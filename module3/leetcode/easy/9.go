package easy

import "strings"

/*
	runtime 3 ms (79.34%)
	memory 3.4 mb (24.72%)
*/

func finalValueAfterOperations(operations []string) int {
	var result int
	for _, op := range operations {
		if strings.Contains(op, "-") {
			result--
		} else {
			result++
		}
	}
	return result
}
