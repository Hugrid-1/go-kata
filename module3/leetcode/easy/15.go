package easy

/*
	runtime 39 ms 76.55%
	memory 7.2 mb 66.90%
*/
type ParkingSystem struct {
	spaces map[int]int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	parkingSystem := ParkingSystem{
		spaces: map[int]int{1: big, 2: medium, 3: small},
	}
	return parkingSystem
}

func (this *ParkingSystem) AddCar(carType int) bool {
	if this.spaces[carType] <= 0 {
		return false
	}
	this.spaces[carType]--
	return true
}
