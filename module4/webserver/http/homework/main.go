package main

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/handlers"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/repository"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	port := ":8080"
	userRepo := repository.TestUserStorage()
	userService := service.NewService(userRepo)
	baseHandler := handlers.NewBaseHandler()
	userHandler := handlers.NewUserHandler(userService)
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", baseHandler.Hello)
	r.Get("/users", userHandler.GetAllUsers)
	r.Get("/users/{id}", userHandler.GetUserByID)
	r.Get("/upload", baseHandler.UploadUI)
	r.Post("/upload", baseHandler.UploadImage)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/http/homework/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
