package medium

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	newListHead := &ListNode{}
	currentNode := newListHead
	sum := 0
	for head != nil {
		if head.Val == 0 && sum != 0 {
			newNode := &ListNode{
				Val:  sum,
				Next: nil,
			}
			currentNode.Next = newNode
			currentNode = currentNode.Next
			sum = 0
		}
		sum += head.Val
		head = head.Next

	}
	newListHead = newListHead.Next
	return newListHead
}
