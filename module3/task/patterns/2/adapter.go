package main

type AirConditionerAdapter struct {
	realAirConditioner *RealAirConditioner
}

func remoteControl(button string, airConditioner *RealAirConditioner) {
	switch button {
	case "off":
		airConditioner.TurnOff()
	case "on":
		airConditioner.TurnOn()
	default:
		return
	}
}
func remoteSet(button string, value int, airConditioner *RealAirConditioner) {
	switch button {
	case "temperature":
		airConditioner.SetTemperature(value)
	default:
		return
	}
}

func (rc AirConditionerAdapter) TurnOn() {
	remoteControl("on", rc.realAirConditioner)
}

func (rc AirConditionerAdapter) TurnOff() {
	remoteControl("off", rc.realAirConditioner)
}

func (rc AirConditionerAdapter) SetTemperature(temp int) {
	remoteSet("temperature", temp, rc.realAirConditioner)
}
