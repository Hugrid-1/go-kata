package easy

/*
	runtime 1 ms 65%
	memory 1.8 mb 63.33 %
*/

// !!! название содержит "2" т.к. задача повторяется с задачей 3!!!
func convertTemperature2(celsius float64) []float64 {
	return []float64{celsius + 273.15, celsius*1.80 + 32.00}
}
