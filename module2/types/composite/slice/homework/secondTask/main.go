package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	fmt.Println("ORIGINAL ARRAY:", users)
	users2 := users[:0]
	for _, item := range users {
		if item.Age <= 40 {
			users2 = append(users2, item)
		}
	}
	fmt.Println("Array without users with age > 40:", users2)
	//users3 := users[:0]
	//for _, item := range users {
	//	if item.Age <= 40 {
	//		users3 = append(users2, item)
	//	}
	//}
	users = users[:len(users)-1]
	users = users[1:]
	fmt.Println("Array without first and last element", users)
}
