package controllers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"net/http"
)

type UserRepository interface {
	Create(user models.User) models.User
	CreateMultiple(users []models.User) []models.User
	Update(user models.User) (models.User, error)
	Delete(username string) error
	GetByName(username string) (models.User, error)
	GetList() []models.User
}

type UserController struct { // Pet контроллер
	storage UserRepository
}

func NewUserController(storage UserRepository) *UserController { // конструктор нашего контроллера
	return &UserController{storage: storage}
}

func (u *UserController) UserCreate(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user = u.storage.Create(user) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(user)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserCreateArray(w http.ResponseWriter, r *http.Request) {
	var users []models.User
	err := json.NewDecoder(r.Body).Decode(&users)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	users = u.storage.CreateMultiple(users) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(users)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserGetByName(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user     models.User
		err      error
		username string
	)

	username = chi.URLParam(r, "Username") // получаем petID из chi router

	user, err = u.storage.GetByName(username)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user     models.User
		err      error
		username string
	)

	username = chi.URLParam(r, "Username") // получаем petID из chi router

	err = u.storage.Delete(username)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err = u.storage.Update(user)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
