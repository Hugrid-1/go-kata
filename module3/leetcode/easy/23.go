package easy

func interpret(command string) string {
	var result string
	for i, ch := range command {
		switch string(ch) {
		case "G":
			result += "G"
		case "(":
			if string(command[i+1]) == ")" {
				result += "o"
			} else {
				result += "al"
			}
		default:
			continue
		}
	}
	return result
}
