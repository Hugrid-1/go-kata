package main

import (
	"fmt"
	"sort"
	"time"
)

type AirConditionerProxy struct {
	airConditionerAdapter *AirConditionerAdapter
	authStatus            bool
	logJournal            map[time.Time]string
}

func NewAirConditionerProxy(authStatus bool) *AirConditionerProxy {
	conditioner := RealAirConditioner{}
	adapter := AirConditionerAdapter{realAirConditioner: &conditioner}
	logJournal := make(map[time.Time]string)
	return &AirConditionerProxy{
		airConditionerAdapter: &adapter,
		authStatus:            authStatus,
		logJournal:            logJournal,
	}
}

func ChangeAuthorizationStatus(proxy *AirConditionerProxy, status bool) {
	proxy.authStatus = status
}

func ShowAuthJournal(proxy *AirConditionerProxy) {
	timeKeys := make([]time.Time, 0, len(proxy.logJournal))
	for k := range proxy.logJournal {
		timeKeys = append(timeKeys, k)
	}
	sort.Slice(timeKeys, func(i, j int) bool {
		return timeKeys[i].Before(timeKeys[j])
	})
	fmt.Println("--------------AUTHENTICATION JOURNAL---------------")

	for _, timeKey := range timeKeys {
		fmt.Printf("[%v] %v \n", timeKey.Format(time.Stamp), proxy.logJournal[timeKey])
	}

	fmt.Println("------------------------END------------------------")
}

func (rc *AirConditionerProxy) TurnOn() {
	if !rc.authStatus {
		fmt.Println("Access denied: authentication required to turn on the air condinioner")
		rc.logJournal[time.Now()] = "Authorization FAILED: turnOn operation"
		return
	}
	rc.logJournal[time.Now()] = "Authorization SUCCESS: turnOn operation"
	rc.airConditionerAdapter.TurnOn()

}

func (rc *AirConditionerProxy) TurnOff() {
	if !rc.authStatus {
		fmt.Println("Access denied: authentication required to turn off the air condinioner")
		rc.logJournal[time.Now()] = "Authorization FAILED: turnOff operation"
		return
	}
	rc.logJournal[time.Now()] = "Authorization SUCCESS: turnOff operation"
	rc.airConditionerAdapter.TurnOff()
}

func (rc *AirConditionerProxy) SetTemperature(temp int) {
	if !rc.authStatus {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
		rc.logJournal[time.Now()] = "Authorization FAILED: SetTemperature operation"
		return
	}
	rc.logJournal[time.Now()] = "Authorization SUCCESS: SetTemperature operation"
	rc.airConditionerAdapter.SetTemperature(temp)
}
