package main

import (
	"os"

	"github.com/mxmCherry/translit/uknational"
	"golang.org/x/text/transform"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	russianText, err := os.ReadFile("tmp/example.txt")
	check(err)
	uk := uknational.ToLatin()
	translitText, _, _ := transform.String(uk.Transformer(), string(russianText))

	resultFile, err := os.Create("tmp/example.processed.txt")
	check(err)
	defer resultFile.Close()

	_, err = resultFile.WriteString(translitText)
	check(err)
}
