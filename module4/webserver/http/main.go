package main

import (
	"fmt"
	"net/http"
)

func handler(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case http.MethodGet:
		_, err := fmt.Fprintf(writer, "Handling GET request")
		if err != nil {
			fmt.Println(err)
			return
		}
	case http.MethodPost:
		_, err := fmt.Fprintf(writer, "Handling POST request")
		if err != nil {
			fmt.Println(err)
			return
		}
	default:
		_, err := fmt.Fprintf(writer, "Ivalid request method", http.StatusMethodNotAllowed)
		if err != nil {
			fmt.Println(err)
			return
		}

	}
}

func main() {
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}
