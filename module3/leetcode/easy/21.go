package easy

func subtractProductAndSum(n int) int {
	tmp := 0
	sum := 0
	product := 1
	for n > 0 {
		tmp = n % 10
		sum += tmp
		product *= tmp
		n = n / 10
	}
	return product - sum
}
