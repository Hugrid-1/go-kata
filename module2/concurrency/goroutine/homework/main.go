package main

import (
	"fmt"
	"time"
)

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "Прошло пол секунды"
		}
	}()
	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "Прошло 2 секунды"
		}
	}()
	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}
}

//
//func main() {
//	urls := []string{
//		"https://google.com/",
//		"https://gitlab.com/",
//		"https://github.com/",
//		"https://yandex.ru/",
//		"https://mail.ru/",
//	}
//
//	var wg sync.WaitGroup
//
//	for _, url := range urls {
//		wg.Add(1)
//		go func(url string) {
//			doHTTP(url)
//			wg.Done()
//		}(url)
//	}
//
//	wg.Wait()
//}
//
//func doHTTP(url string) {
//	t := time.Now()
//
//	resp, err := http.Get(url)
//	if err != nil {
//		fmt.Printf("Failed to get <%s>: %s\n", url, err.Error())
//	}
//
//	defer resp.Body.Close()
//
//	fmt.Printf("<%s> - Status code [%d] - Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
//
//}

//
//func main() {
//	message := make(chan string, 2) //небуфиризированный канал
//	message <- "hello"
//	message <- "world"
//	fmt.Println(<-message) //эта строчка нужна чтобы освободить буфер
//	message <- "!"
//
//	fmt.Println(<-message)
//	fmt.Println(<-message)
//}

//func main() {
//	message := make(chan string)
//
//	go func() {
//		for i := 1; i <= 10; i++ {
//			message <- fmt.Sprintf("%d", i)
//			time.Sleep(time.Millisecond * 500)
//		}
//		close(message)
//	}()
//	for msg := range message {
//		//msg, open := <-message //проверка на открытость канала
//
//		fmt.Println(msg)
//	}
//
//}

//func main() {
//	t := time.Now()
//	rand.Seed(t.UnixNano())
//
//	go parseUrl("https://platform.kata.academy/user/courses/3/8/1/8")
//	parseUrl("https://platform.kata.academy/user/232313")
//
//	fmt.Printf("Parsing completed. Time Elapesed: %f seconds", time.Since(t).Seconds())
//}

//func parseUrl(url string) {
//	for i := 0; i < 5; i++ {
//		latency := rand.Intn(500) + 500
//		time.Sleep(time.Duration(latency) * time.Microsecond)
//		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)
//	}
//}
