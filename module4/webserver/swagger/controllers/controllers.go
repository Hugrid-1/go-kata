package controllers

import "gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/repository"

type Controllers struct {
	*PetController
	*UserController
	*StoreController
}

func NewControllers(Repository *repository.Repository) *Controllers {
	return &Controllers{
		PetController:   NewPetController(Repository.PetStorager),
		UserController:  NewUserController(Repository.UserStorager),
		StoreController: NewStoreController(Repository.StoreStorager),
	}
}
