package main

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

func printMemStats(message string, rtm runtime.MemStats) {
	fmt.Println("\n===", message, "===")
	fmt.Println("Mallocs: ", rtm.Mallocs)
	fmt.Println("Frees: ", rtm.Frees)
	fmt.Println("LiveObjects: ", rtm.Mallocs-rtm.Frees)
	fmt.Println("PauseTotalNs: ", rtm.PauseTotalNs)
	fmt.Println("NumGC: ", rtm.NumGC)
	fmt.Println("LastGC: ", time.UnixMilli(int64(rtm.LastGC/1_000_000)))
	fmt.Println("HeapObjects: ", rtm.HeapObjects)
	fmt.Println("HeapAlloc: ", rtm.HeapAlloc)
}

func main() {
	var rtm runtime.MemStats
	runtime.ReadMemStats(&rtm)
	printMemStats("Start", rtm)

	a := make([]int64, 0)
	var i int64
	for i = 0; i < 10_000_000; i++ {
		a = append(a, i)
	}
	_ = a

	runtime.ReadMemStats(&rtm)
	printMemStats("After 10 million int64 appends", rtm)

	var b []string
	var j int
	for j = 0; j < 10_000_000; j++ {
		b = append(b, "hello"+strconv.Itoa(j))
	}
	_ = b

	runtime.ReadMemStats(&rtm)
	printMemStats("After 10 million string appends", rtm)

	runtime.GC()

	runtime.ReadMemStats(&rtm)
	printMemStats("After forced GC", rtm)
}
