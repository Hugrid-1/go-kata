package router

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/controllers"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/repository"
	"net/http"
)

func NewRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	Repository := repository.NewRepository()
	Controllers := controllers.NewControllers(Repository)
	//petController := controllers.NewPetController()
	//storeController := controllers.NewStoreController()
	//userController := controllers.NewUserController()

	//Pet routes
	r.Post("/pet", Controllers.PetCreate)
	r.Put("/pet", Controllers.PetUpdate)
	r.Get("/pet/{petID}", Controllers.PetGetByID)
	r.Delete("/pet/{petID}", Controllers.PetDelete)
	r.Post("/pet/{petID}", Controllers.PetEdit)
	r.Post("/pet/{petID}/uploadImage", Controllers.PetUploadImage)
	r.Get("/pet/findByStatus", Controllers.PetGetByStatus)

	//Store routes
	r.Get("/store/inventory", Controllers.StoreGetStatusCountMap)
	r.Get("/store/order/{orderID}", Controllers.OrderGetByID)
	r.Delete("/store/order/{orderID}", Controllers.OrderDelete)
	r.Post("/store/order", Controllers.OrderCreate)

	//User routes
	r.Post("/user", Controllers.UserCreate)
	r.Post("/user/createWithArray", Controllers.UserCreateArray)
	r.Post("/user/createWithList", Controllers.UserCreateArray)
	r.Get("/user/{Username}", Controllers.UserGetByName)
	r.Put("/user", Controllers.UserUpdate)
	r.Delete("/user/{Username}", Controllers.UserDelete)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("module4/webserver/swagger/public"))).ServeHTTP(w, r)
	})
	return r
}
