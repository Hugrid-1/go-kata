package main

import (
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
)

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//	  200: petAddResponse

//swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body models.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route GET /pet/{PetID} pet petGetByIDRequest
// Найти питомца по ID.
// responses:
//	  200: petGetByIDResponse

//swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// in:path
	PetID int
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route POST /pet/{PetID}/uploadImage pet petUploadPhotoRequest
// Загрузить изображение
// responses:
//	  200: petUploadPhotoResponse

//swagger:parameters petUploadPhotoRequest
type petUploadPhotoRequest struct {
	// ID питомца
	// in:path
	PetID int
	// файл для загрузки
	// in: formData
	// type: file
	// format: binary
	// swagger:file
	File string
}

// swagger:response petUploadPhotoResponse
type petUploadPhotoResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route POST /pet/{PetID} pet petUpdateRequest
// Обновить информацию о питомце.
// responses:
//	  200: petUpdateResponse

//swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// ID питомца
	// in:path
	PetID int
	// Новое имя
	// in:formData
	// required:false
	Name string
	// Новый статус
	// in:formData
	// required:false
	Status string
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route PUT /pet pet petPutRequest
// Обновление питомца.
// responses:
//	  200: petPutResponse

//swagger:parameters petPutRequest
type petPutRequest struct {
	// in:body
	Body models.Pet
}

// swagger:response petPutResponse
type petPutResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route DELETE /pet/{PetID} pet petDeleteRequest
// Удалить питомца.
// swagger

//swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID питомца
	// in:path
	PetID int
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Получить питомцев со статусом.
// responses:
//	  200: petFindByStatusResponse

//swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// ID питомца
	// in:query
	Status []string
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	// type:array
	// collectionFormat:multi
	// in:body

	Body []models.Pet
}

// swagger:route POST /store/order store orderAddRequest
// Добавить заказ.
// responses:
//	  200: orderAddResponse

//swagger:parameters orderAddRequest
type orderAddRequest struct {
	// in:body
	Body models.Store
}

// swagger:response orderAddResponse
type orderAddResponse struct {
	// in:body
	Body models.Store
}

// swagger:route GET /store/order/{OrderID} store orderGetRequest
// Получить заказ по ID.
// responses:
//	  200: orderGetResponse

//swagger:parameters orderGetRequest
type orderGetRequest struct {
	// in:path
	OrderID int
}

// swagger:response orderAddResponse
type orderGetResponse struct {
	// in:body
	Body models.Store
}

// swagger:route DELETE /store/order/{OrderID} store orderDeleteRequest
// Удалить заказ.
// responses:

//swagger:parameters orderDeleteRequest
type orderDeleteRequest struct {
	// in:path
	OrderID int
}

// swagger:route GET /store/inventory store storeInventoryRequest
// Получить статиску статусов питомцев.
// responses:
//	  200: storeInventoryResponse

// swagger:response storeInventoryResponse
type storeInventoryResponse struct {
	// in:body
	Body map[string]int
}

// swagger:route POST /user user userAddRequest
// Добавить заказ.
// responses:
//	  200: userAddResponse

//swagger:parameters userAddRequest
type userAddRequest struct {
	// in:body
	Body models.User
}

// swagger:response userAddResponse
type userAddResponse struct {
	// in:body
	Body models.User
}

// swagger:route GET /user/{Username} user userGetRequest
// Получить пользователя по username.
// responses:
//	  200: userGetResponse

//swagger:parameters userGetRequest
type userGetRequest struct {
	// in:path
	Username string
}

// swagger:response userGetResponse
type userGetResponse struct {
	// in:body
	Body models.User
}

// swagger:route PUT /user user userUpdateRequest
// Обновить пользователя.
// responses:
//	  200: userUpdateResponse

//swagger:parameters userUpdateRequest
type userUpdateRequest struct {
	// in:body
	Body models.User
}

// swagger:response userUpdateResponse
type userUpdateResponse struct {
	// in:body
	Body models.User
}

// swagger:route DELETE /user/{Username} user userDeleteRequest
// Удалить пользователя.
// responses:
//	  200: userDeleteResponse

//swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	// in:path
	Username string
}

// swagger:response userDeleteResponse
type userDeleteResponse struct {
	// in:body
	Body models.User
}

// swagger:route POST /user/createWithArray user userAddArrayRequest
// Добавить массив пользователей.
// responses:
//	  200: userAddArrayResponse

//swagger:parameters userAddArrayRequest
type userAddArrayRequest struct {
	// in:body
	Body []models.User
}

// swagger:response userAddArrayResponse
type userAddArrayResponse struct {
	// in:body
	Body []models.User
}

// swagger:route POST /user/createWithList user userAddArrayRequest
// Добавить лист пользователей.
// responses:
//	  200: userAddArrayResponse
