module gitlab.com/Hugrid-1/go-kata

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/mxmCherry/translit v1.0.0
	github.com/stretchr/testify v1.8.2
	gitlab.com/Hugrid-1/greet v0.0.0-20230109200900-a9b9177ba2c8
	golang.org/x/sync v0.1.0
	golang.org/x/text v0.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.6 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_golang v1.15.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
