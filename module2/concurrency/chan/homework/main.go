package main

import (
	"fmt"
	"sync"
	"time"
)

const timeout = time.Second * 30

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)
	ticker := time.NewTicker(1 * time.Second)
	done := make(chan bool)
	go func() {

		for _, num := range []int{1, 2, 3} {
			a <- num
		}
	}()

	go func() {
		for _, num := range []int{20, 10, 30} {
			b <- num
		}
	}()

	go func() {
		for _, num := range []int{300, 200, 100} {
			c <- num
		}
	}()

	//Даже если по сути решение верное,укажите пожалуйста как нужно было более правильно сделать
	go func() {
		time.Sleep(timeout)
		done <- true
		ticker.Stop()
	}()

	go func() {
		for {
			select {
			case <-done:
				close(a)
				close(b)
				close(c)
				return
			case <-ticker.C:
				continue
			}
		}
	}()

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}

	fmt.Printf("Program stop work after %v", timeout)

}
