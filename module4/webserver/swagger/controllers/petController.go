package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type PetRepository interface {
	Create(pet models.Pet) models.Pet
	Update(pet models.Pet) (models.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (models.Pet, error)
	GetByStatus(status ...string) ([]models.Pet, error)
	AddPhotoUrl(petID int, filepath string) error
	GetList() []models.Pet
}

type PetController struct { // Pet контроллер
	storage PetRepository
}

func NewPetController(storage PetRepository) *PetController { // конструктор нашего контроллера
	return &PetController{storage: storage}
}

func (s *PetController) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet models.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *server.go.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet = s.storage.Create(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в server.go.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *PetController) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      models.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err = s.storage.GetByID(petID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *PetController) PetDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.storage.Delete(petID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (s *PetController) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router
	// Get handler for filename
	file, handler, err := r.FormFile("File")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
	}
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)

	defer file.Close()

	fmt.Fprintf(w, "Successfully Uploaded File\n")
	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	filepath := fmt.Sprintf("./module4/webserver/swagger/public/%v", handler.Filename)
	// Create file
	imageFile, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
	defer imageFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(imageFile, file); err != nil {
		fmt.Println(err)
		return
	}

	err = s.storage.AddPhotoUrl(petID, filepath)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (s *PetController) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var pet models.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *server.go.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err = s.storage.Update(pet) // создаем запись в нашем storage
	if err != nil {
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в server.go.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *PetController) PetEdit(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err          error
		petIDRaw     string
		petNameRaw   string
		petStatusRaw string
		petID        int
		pet          models.Pet
	)
	petIDRaw = chi.URLParam(r, "petID")
	petNameRaw = r.FormValue("Name")
	petStatusRaw = r.FormValue("Status")
	fmt.Println(petNameRaw, petStatusRaw)
	petID, err = strconv.Atoi(petIDRaw)
	pet, err = s.storage.GetByID(petID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	if petNameRaw != "" {
		pet.Name = petNameRaw
	}
	if petStatusRaw != "" {
		pet.Status = petStatusRaw
	}

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err = s.storage.Update(pet)
	if err != nil {
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в server.go.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *PetController) PetGetByStatus(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pets           []models.Pet
		err            error
		petStatusArray []string
	)

	petStatusArray = r.URL.Query()["Status"]
	if len(petStatusArray) == 1 {
		petStatusArray = strings.Split(petStatusArray[0], ",")
	}
	fmt.Println(len(petStatusArray))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pets, err = s.storage.GetByStatus(petStatusArray...)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pets)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
