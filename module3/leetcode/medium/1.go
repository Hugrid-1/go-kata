package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	_, sum := sumLeaves(root)
	return sum
}

func sumLeaves(root *TreeNode) (depth, sum int) {
	if root.Left == nil && root.Right == nil {
		depth++
		sum = root.Val
		return
	}

	if root.Left != nil && root.Right != nil {
		deathLeft, sumLeft := sumLeaves(root.Left)
		deathRight, sumRight := sumLeaves(root.Right)
		if deathLeft == deathRight {
			depth = deathLeft + 1
			sum = sumLeft + sumRight
			return
		} else if deathLeft > deathRight {
			depth = deathLeft + 1
			sum = sumLeft
			return
		} else {
			depth = deathRight + 1
			sum = sumRight
			return
		}
	} else if root.Left != nil {
		deathLeft, sumLeft := sumLeaves(root.Left)
		depth = deathLeft + 1
		sum = sumLeft
		return
	} else {
		deathRight, sumRight := sumLeaves(root.Right)
		depth = deathRight + 1
		sum = sumRight
		return
	}
}
