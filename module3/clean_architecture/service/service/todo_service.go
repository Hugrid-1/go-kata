package service

import (
	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/service/model"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	SortTodos() ([]model.Todo, error) // sort by status
	CreateTodo(title string) error
	EditTodo(todo model.Todo, newTitle string, newDescription string, newStatus string) //Заменил функцию смены статуса на изменение всей задачи т.к. это дает больше возможностей для редактировани
	RemoveTodo(todo model.Todo) error
}

type TodoRepository interface {
	GetTasks() ([]model.Todo, error)
	CreateTask(task model.Todo) (model.Todo, error)
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
}

type todoService struct {
	repository TodoRepository
}

func NewTodoService(repository TodoRepository) *todoService {
	return &todoService{
		repository: repository,
	}
}

func (s *todoService) ListTodos() ([]model.Todo, error) {
	if tasks, err := s.repository.GetTasks(); err == nil {
		var todoSlice = make([]model.Todo, len(tasks))
		for i, task := range tasks {
			newTodo := model.Todo{
				ID:          task.ID,
				Title:       task.Title,
				Description: task.Description,
				Status:      task.Status,
			}
			todoSlice[i] = newTodo
		}
		return todoSlice, err
	} else {
		return []model.Todo{}, err
	}
}

func (s *todoService) CreateTodo(title string) error {
	newTask := model.Todo{
		Title:  title,
		Status: "Need to do"}
	_, err := s.repository.CreateTask(newTask)
	if err != nil {
		return err
	}
	return err
}

func (s *todoService) EditTodo(todo model.Todo, newTitle string, newDescription string, newStatus string) error {
	task := model.Todo{
		ID:          todo.ID,
		Title:       newTitle,
		Description: newDescription,
		Status:      newStatus,
	}
	_, err := s.repository.UpdateTask(task)
	if err != nil {
		return err
	}
	return err
}

func (s *todoService) RemoveTodo(todo model.Todo) error {
	err := s.repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return err
}
