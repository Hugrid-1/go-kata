package easy

func countGoodTriplets(arr []int, a int, b int, c int) int {
	counterGoodTriplets := 0
	for i := 0; i < len(arr)-2; i++ {
		for j := i + 1; j < len(arr)-1; j++ {
			for k := j + 1; k < len(arr); k++ {
				first := arr[i] - arr[j]
				if first < 0 {
					first = first * (-1)
				}
				second := arr[j] - arr[k]
				if second < 0 {
					second = second * (-1)
				}
				third := arr[i] - arr[k]
				if third < 0 {
					third = third * (-1)
				}

				if first <= a && second <= b && third <= c {
					counterGoodTriplets++
				}
			}
		}
	}
	return counterGoodTriplets
}
