package main

import "testing"

func TestDiscountCalculator_Calculate(t *testing.T) {
	type fields struct {
		PricingStrategy PricingStrategy
	}
	type args struct {
		order Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   float64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := &DiscountCalculator{
				PricingStrategy: tt.fields.PricingStrategy,
			}
			if got := dc.Calculate(tt.args.order); got != tt.want {
				t.Errorf("Calculate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDiscountCalculator_setStrategy(t *testing.T) {
	type fields struct {
		PricingStrategy PricingStrategy
	}
	type args struct {
		strategy PricingStrategy
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := &DiscountCalculator{
				PricingStrategy: tt.fields.PricingStrategy,
			}
			dc.setStrategy(tt.args.strategy)
		})
	}
}

func TestRegularPricing_Calculate(t *testing.T) {
	type args struct {
		order Order
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RegularPricing{}
			if got := r.Calculate(tt.args.order); got != tt.want {
				t.Errorf("Calculate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSalePricing_Calculate(t *testing.T) {
	type fields struct {
		Discount float64
	}
	type args struct {
		order Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   float64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := SalePricing{
				Discount: tt.fields.Discount,
			}
			if got := r.Calculate(tt.args.order); got != tt.want {
				t.Errorf("Calculate() = %v, want %v", got, tt.want)
			}
		})
	}
}
