package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File     io.ReadWriter
	FilePath string //добавил путь до файла так как в описаниях функций указывается r.FilePath + это как мне кажется позволяет удобней реализовать некоторые моменты
	Users    []User //для более быстрой и простой реализации поиска
}

func NewUserRepository(file io.ReadWriter, filePath string) *UserRepository {
	return &UserRepository{
		File:     file,
		FilePath: filePath,
		Users:    make([]User, 0),
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	file, err := os.OpenFile(r.FilePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	if recordContent, ok := record.(User); ok {
		for _, user := range r.Users {
			if user.ID == recordContent.ID {
				return fmt.Errorf("user with ID %v already exist", recordContent.ID)
			}
		}
		r.Users = append(r.Users, recordContent)
		newUsersData, err := json.Marshal(r.Users)
		if err != nil {
			return err
		}
		err = file.Truncate(0)
		if err != nil {
			return err
		}
		_, err = file.Seek(0, 0)
		if err != nil {
			return err
		}
		_, err = file.Write(newUsersData)
		if err != nil {
			return err
		}
		err = file.Sync()
		if err != nil {
			return err
		}
		return err

	} else {
		return fmt.Errorf("record is not User")
	}

}

func (r *UserRepository) Find(id int) (interface{}, error) {
	if len(r.Users) == 0 {
		return User{}, fmt.Errorf("Repository is empty")
	}
	for _, user := range r.Users {
		if user.ID == id {
			return user, nil
		}
	}
	return User{}, fmt.Errorf("Not found users with id == %v", id)
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	result := make([]interface{}, 0)
	if len(r.Users) == 0 {
		return result, fmt.Errorf("Repository is empty")
	}
	for i := range r.Users {
		result = append(result, r.Users[i])
	}

	return result, nil
}
