package main

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) float64
}
type Order struct {
	name  string
	price float64
	count int
}
type RegularPricing struct {
}

func (r RegularPricing) Calculate(order Order) float64 {
	return order.price * float64(order.count)
}

type SalePricing struct {
	Discount float64
}

func (r SalePricing) Calculate(order Order) float64 {
	result := (1 - r.Discount/100) * (order.price * float64(order.count))
	return result
}

type DiscountCalculator struct {
	PricingStrategy PricingStrategy
}

func (dc *DiscountCalculator) setStrategy(strategy PricingStrategy) {
	dc.PricingStrategy = strategy
}

func (dc *DiscountCalculator) Calculate(order Order) float64 {
	return dc.PricingStrategy.Calculate(order)
}

func main() {
	order := Order{
		name:  "apple",
		price: 18,
		count: 75,
	}

	regularPrice := RegularPricing{}
	regularCustomerDiscount := SalePricing{Discount: 5.0}
	firstOrderDiscount := SalePricing{Discount: 10.0}
	springDiscount := SalePricing{Discount: 15.0}
	newYearDiscount := SalePricing{Discount: 30.0}
	var strategies = []PricingStrategy{regularPrice, regularCustomerDiscount, firstOrderDiscount, springDiscount, newYearDiscount}

	discountCalculator := DiscountCalculator{}
	for _, strategy := range strategies {
		discountCalculator.setStrategy(strategy)
		result := discountCalculator.Calculate(order)
		fmt.Printf("Total cost with %T: %v\n", strategy, result)
	}
}
