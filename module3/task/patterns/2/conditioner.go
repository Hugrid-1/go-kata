package main

import "fmt"

// AirConditioner is the commandInterface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
	temperature int
}

func (rc RealAirConditioner) TurnOn() {
	fmt.Println("Turning ON air conditioner")
}

func (rc RealAirConditioner) TurnOff() {
	fmt.Println("Turning OFF air conditioner")
}

func (rc RealAirConditioner) SetTemperature(temp int) {
	if temp > 30 {
		fmt.Println("Setting air conditioner temperature FAILED : maximum temperature is 30")
		return
	}
	if temp < -10 {
		fmt.Println("Setting air conditioner temperature FAILED : minimum temperature is -10")
		return
	}
	rc.temperature = temp
	fmt.Printf("Setting air conditioner temperature to %v\n", rc.temperature)
}
