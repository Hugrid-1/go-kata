package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"time"
)

const (
	uploadTemplate = `<!DOCTYPE html>
<html>
<head>
   	<title>Загрузка файлов</title>
</head>
<body>
<form enctype="multipart/form-data" action="http://localhost:8080/upload" method="post">
	<input type="file" name="file"/>
	<input type="hidden" name="token" value="{{.}}"/>
	<input type="submit" value="upload" />
</form>
</body>
</html>
`
)

type BaseHandler struct { // Pet контроллер
}

func NewBaseHandler() *BaseHandler { // конструктор нашего контроллера
	return &BaseHandler{}
}

func (b *BaseHandler) Hello(w http.ResponseWriter, r *http.Request) {
	message := "Привет. Это приветственное сообщение"
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(message)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (b *BaseHandler) UploadImage(w http.ResponseWriter, r *http.Request) {

	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
	}
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)

	defer file.Close()

	fmt.Fprintf(w, "Successfully Uploaded File\n")
	filepath := fmt.Sprintf("./module4/webserver/http/homework/public/%v", handler.Filename)
	// Create file
	imageFile, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
	defer imageFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(imageFile, file); err != nil {
		fmt.Println(err)
		return
	}

}

func (b *BaseHandler) UploadUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("upload").Parse(uploadTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}
