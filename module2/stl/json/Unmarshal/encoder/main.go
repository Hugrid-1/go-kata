package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	buf := new(bytes.Buffer)
	bufEncoder := json.NewEncoder(buf)

	_ = bufEncoder.Encode(Person{"Ross Geller", 28})
	_ = bufEncoder.Encode(Person{"Vladimir Zolkin", 20})
	_ = bufEncoder.Encode(Person{"Ivan Ivanov", 34})

	fmt.Println(buf)
}
