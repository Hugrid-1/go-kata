package easy

/*
	runtime 0 ms 100%
	memory 1.8 mb 58.42%
*/

func smallestEvenMultiple(n int) int {
	if n%2 == 0 {
		return n
	}
	return n * 2
}
