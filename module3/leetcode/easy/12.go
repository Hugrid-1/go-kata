package easy

/*
	runtime 0 ms 100%
	memory 1.9 mb 38.46%
*/

func numIdenticalPairs(nums []int) int {
	pairsCount := 0

	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				pairsCount++
			}
		}
	}

	return pairsCount
}
