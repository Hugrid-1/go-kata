package easy

func balancedStringSplit(s string) int {
	var resultCounter, counterL, counterR int
	for _, letter := range s {
		if letter == 'L' {
			counterL++
		} else if letter == 'R' {
			counterR++
		}
		if counterL == counterR {
			resultCounter++
		}
	}

	return resultCounter
}
