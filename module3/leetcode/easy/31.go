package easy

import "sort"

func sortPeople(names []string, heights []int) []string {
	heightsMap := make(map[int]string, len(heights))
	result := make([]string, len(names))
	for i, height := range heights {
		heightsMap[height] = names[i]
	}
	sort.Ints(heights)
	for i, j := len(heights)-1, 0; i >= 0; i-- {
		result[j] = heightsMap[heights[i]]
		j++
	}
	return result
}
