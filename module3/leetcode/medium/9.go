package medium

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var result = make([]bool, len(l))
	for i, value := range l {
		subArray := nums[value : r[i]+1]
		var tempArray = make([]int, len(subArray))
		copy(tempArray, subArray)
		sort.Ints(tempArray)
		result[i] = checkArray(tempArray)
	}

	return result
}

func checkArray(arr []int) bool {
	d := arr[0] - arr[1]
	for i := 1; i < len(arr)-1; i++ {
		if arr[i]-arr[i+1] != d {
			return false
		}
	}
	return true
}
