package medium

func xorQueries(arr []int, queries [][]int) []int {
	var xArr []int = make([]int, len(arr))
	var result []int

	xArr[0] = arr[0]
	for i := 1; i < len(arr); i++ {
		xArr[i] = xArr[i-1] ^ arr[i]
	}
	for _, v := range queries {
		if v[0] == 0 {
			result = append(result, xArr[v[1]])
		} else {
			result = append(result, xArr[v[0]-1]^xArr[v[1]])
		}
	}
	return result
}
