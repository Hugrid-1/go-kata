package medium

func balanceBST(root *TreeNode) *TreeNode {
	var nodes []*TreeNode
	passNodes(root, &nodes)
	nodeCount := len(nodes)
	return buildTree(nodes, 0, nodeCount-1)
}

func passNodes(node *TreeNode, arr *[]*TreeNode) {
	if node == nil {
		return
	}
	passNodes(node.Left, arr)
	*arr = append(*arr, node)
	passNodes(node.Right, arr)
}

func buildTree(nodes []*TreeNode, start, end int) *TreeNode {
	if start > end {
		return nil
	}
	mid := (start + end) / 2
	root := nodes[mid]

	root.Left = buildTree(nodes, start, mid-1)
	root.Right = buildTree(nodes, mid+1, end)

	return root
}
