package repository

import (
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"reflect"
	"sync"
	"testing"
)

type storeFields struct {
	data               []*models.Store
	primaryKeyIDx      map[int]*models.Store
	autoIncrementCount int
	Mutex              sync.Mutex
}

func getStoreTestData() storeFields {
	testFields := storeFields{
		data: []*models.Store{
			{ID: 0, PetID: 0, Quantity: 0, ShipDate: "s", Status: "s", Complete: false},
			{ID: 1, PetID: 1, Quantity: 0, ShipDate: "s2", Status: "s2", Complete: false},
			{ID: 2, PetID: 2, Quantity: 0, ShipDate: "s3", Status: "s3", Complete: true},
		},
		primaryKeyIDx: map[int]*models.Store{0: {ID: 0, PetID: 0, Quantity: 0, ShipDate: "s", Status: "s", Complete: false},
			1: {ID: 1, PetID: 1, Quantity: 0, ShipDate: "s2", Status: "s2", Complete: false},
			2: {ID: 2, PetID: 2, Quantity: 0, ShipDate: "s3", Status: "s3", Complete: true}},
		autoIncrementCount: 3,
		Mutex:              sync.Mutex{},
	}
	return testFields
}

func TestStoreStorage_Create(t *testing.T) {
	testData := getStoreTestData()
	type args struct {
		order models.Store
	}
	tests := []struct {
		name   string
		fields storeFields
		args   args
		want   models.Store
	}{
		{
			name:   "base test",
			fields: testData,
			args:   args{models.Store{ID: 0, PetID: 0, Quantity: 0, ShipDate: "s", Status: "s", Complete: false}},
			want:   models.Store{ID: 3, PetID: 0, Quantity: 0, ShipDate: "s", Status: "s", Complete: false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := s.Create(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	testData := getStoreTestData()
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  storeFields
		args    args
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{orderID: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if err := p.Delete(tt.args.orderID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
			if len(tt.fields.data) == len(p.data) {
				t.Errorf("Not deleted")
			}
		})
	}
}

func TestStoreStorage_GetStatusCountMap(t *testing.T) {
	testData := getStoreTestData()
	tests := []struct {
		name   string
		fields storeFields
		want   map[string]int
	}{
		{
			name:   "base test",
			fields: testData,
			want: map[string]int{
				"s":  1,
				"s2": 1,
				"s3": 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			if got := s.GetStatusCountMap(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatusCountMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	testData := getStoreTestData()
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  storeFields
		args    args
		want    models.Store
		wantErr bool
	}{
		{
			name:    "base test",
			fields:  testData,
			args:    args{orderID: 0},
			want:    *testData.primaryKeyIDx[0],
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				Mutex:              tt.fields.Mutex,
			}
			got, err := s.GetByID(tt.args.orderID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}
