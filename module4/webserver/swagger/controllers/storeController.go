package controllers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/Hugrid-1/go-kata/module4/webserver/swagger/models"
	"net/http"
	"strconv"
)

type StoreRepository interface {
	Create(order models.Store) models.Store
	Delete(orderID int) error
	GetByID(orderID int) (models.Store, error)
	GetStatusCountMap() map[string]int
}

type StoreController struct { // Pet контроллер
	storage StoreRepository
}

func NewStoreController(storage StoreRepository) *StoreController { // конструктор нашего контроллера
	return &StoreController{storage: storage}
}

func (s *StoreController) OrderCreate(w http.ResponseWriter, r *http.Request) {
	var order models.Store
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order = s.storage.Create(order) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(order)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreController) OrderGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		order      models.Store
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID") // получаем orderID из chi router

	orderID, err = strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err = s.storage.GetByID(orderID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreController) OrderDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID") // получаем petID из chi router

	orderID, err = strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.storage.Delete(orderID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (s *StoreController) StoreGetStatusCountMap(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		countMap map[string]int
	)
	countMap = s.storage.GetStatusCountMap()
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(countMap)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
