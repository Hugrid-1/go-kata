package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)

	request, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(request)

	parts := strings.Split(request, " ")
	method := parts[0]
	path := parts[1]
	if path != "/" {
		_, err = fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	switch {
	case method == "POST" && path == "/upload":
		file, err := os.Create("uploaded_file.txt")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()

		_, err = io.Copy(file, reader)
		if err != nil {
			fmt.Println(err)
			return
		}

		_, err = fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nFile uploaded successfully!")
		if err != nil {
			return
		}
	case method == "GET" && path == "/":
		m := "HTTP/1.1 200 OK\n"
		m = m + "Server: nginx/1.19.2\n"
		m = m + "Date: Mon, 19 Oct 2020 13:13:29 GMT\n"
		m = m + "Content-Type: text/html\n"
		m = m + "Content-Length: 98\n" // указываем размер контента
		m = m + "Last-Modified: Mon, 19 Oct 2020 13:13:13 GMT\n"
		m = m + "Connection: keep-alive\n"
		m = m + "ETag: \"5f8d90e9-62\"\n"
		m = m + "Accept-Ranges: bytes\n"
		m = m + "\n"
		m = m + "<!DOCTYPE html>\n"
		m = m + "<html>\n"
		m = m + "<head>\n"
		m = m + "<title>Webserver</title>\n"
		m = m + "</head>\n"
		m = m + "<body>\n"
		m = m + "hello world\n"
		m = m + "</body>\n"
		m = m + "</html>\n"
		_, err = conn.Write([]byte(m))
		if err != nil {
			fmt.Println(err)
			return
		}
	default:
		_, err = fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer listener.Close()
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		go handleConnection(conn)
	}
}
