package main

import (
	"testing"
)

// insertXInterfaceMap - для добавления X элементов в Map[commandInterface]int
func insertXInterfaceMap(x int, b *testing.B) {
	// Инициализируем Map и вставляем X элементов
	testmap := make(map[interface{}]int, 0)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}

// BenchmarkInsertInterfaceMap1000000 тестирует скорость вставки 1000000 целых чисел в карту.
func BenchmarkInsertInterfaceMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(1000000, b)
	}
}

// BenchmarkInsertInterfaceMap100000 тестирует скорость вставки 100000 целых чисел в карту.
func BenchmarkInsertInterfaceMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(100000, b)
	}
}

// BenchmarkInsertInterfaceMap10000 тестирует скорость вставки 10000 целых чисел в карту.
func BenchmarkInsertInterfaceMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(10000, b)
	}
}

// BenchmarkInsertInterfaceMap1000 тестирует скорость вставки 1000 целых чисел в карту.
func BenchmarkInsertInterfaceMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(1000, b)
	}
}

// BenchmarkInsertInterfaceMap100 тестирует скорость вставки 100 целых чисел в карту.
func BenchmarkInsertInterfaceMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(100, b)
	}
}
