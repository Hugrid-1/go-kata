package main

import (
	"encoding/json"
	"log"
	"os"

	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/cli/commandInterface"
	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/cli/model"
	repository "gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/cli/repository"
	"gitlab.com/Hugrid-1/go-kata/module3/clean_architecture/cli/service"
)

func main() {
	r := createTestRepo()
	s := service.NewTodoService(&r)
	cli := commandInterface.NewCli(s)
	cli.StartWork()
	for {
		if !cli.GetStatus() {
			break
		}
	}

}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func createTestRepo() repository.FileTaskRepository {
	filepath := "./testRepository.json"
	testTodoData := []model.Todo{
		{ID: 1, Title: "wash the dishes", Description: "1test description", Status: "Need to do"},
		{ID: 2, Title: "shopping", Description: "2test description", Status: "Need to do"},
		{ID: 3, Title: "go to store", Description: "3test description", Status: "finished"},
		{ID: 4, Title: "walk the dog", Description: "4test description", Status: "Need to do"},
	}
	file, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0666)
	check(err)
	err = file.Truncate(0)
	check(err)
	_, err = file.Seek(0, 0)
	check(err)
	testData, err := json.Marshal(testTodoData)
	check(err)
	_, err = file.Write(testData)
	check(err)
	repository := repository.NewFIleTaskRepository(filepath)
	return *repository
}
