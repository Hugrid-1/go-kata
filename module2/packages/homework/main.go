package main

import (
	"fmt"
	"gitlab.com/Hugrid-1/greet"
)

func main() {
	greet.Hello()

	fmt.Println(greet.Shark)

	oct := greet.Octopus{
		Name:  "Lessy",
		Color: "blue",
	}

	fmt.Println(oct.String())

	oct.Reset()

	fmt.Println(oct.String())
}
