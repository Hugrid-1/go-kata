package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeUint()
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var uint8Number uint8 = 1 << 7
	var min8 = int8(uint8Number)
	uint8Number--
	var max8 = int8(uint8Number)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, unsafe.Sizeof(min8), "bytes")

	var uint16Number uint16 = 1 << 15
	var min16 = int16(uint16Number)
	uint16Number--
	var max16 = int16(uint16Number)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, unsafe.Sizeof(min16), "bytes")

	var uint32Number uint32 = 1 << 31
	var min32 = int32(uint32Number)
	uint32Number--
	var max32 = int32(uint32Number)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, unsafe.Sizeof(min32), "bytes")

	var uint64Number uint64 = 1 << 63
	var min64 = int64(uint64Number)
	uint64Number--
	var max64 = int64(uint64Number)
	fmt.Println("int64 min value:", min64, "int64max value:", max64, unsafe.Sizeof(min64), "bytes")

	fmt.Println("=== END type int ===")
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size is:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size is:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size is:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size is:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}
