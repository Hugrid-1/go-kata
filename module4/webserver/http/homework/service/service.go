package service

import "gitlab.com/Hugrid-1/go-kata/module4/webserver/http/homework/models"

type Service interface {
	ListUsers() []models.User
	GetUser(id int) (models.User, error)
}

type UserRepository interface {
	GetAll() []models.User
	GetByID(id int) (models.User, error)
}

type service struct {
	repository UserRepository
}

func NewService(repository UserRepository) *service {
	return &service{
		repository: repository,
	}
}

func (s service) ListUsers() []models.User {
	return s.repository.GetAll()
}

func (s service) GetUser(id int) (models.User, error) {
	var user models.User
	user, err := s.repository.GetByID(id)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}
