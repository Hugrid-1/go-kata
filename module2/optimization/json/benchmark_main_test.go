package main

import (
	"testing"
)

var (
	pets Pets
	data []byte
)

func BenchmarkStandartJsonUnMarshal(b *testing.B) {
	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
	}
}
func BenchmarkStandartJsonMarshal(b *testing.B) {
	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterUnMarshal(b *testing.B) {
	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
	}
}
func BenchmarkJsonIterMarshal(b *testing.B) {
	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
	}
}
