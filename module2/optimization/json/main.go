package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

type Pets []Pet

var jsonData = []byte(`
	[
  {
    "id": 6739257,
    "category": {
      "id": 10,
      "name": "string"
    },
    "name": "Changed Kitty Price",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 10,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 1005003,
    "category": {
      "id": 1005003,
      "name": "Direwolf"
    },
    "name": "Ghost",
    "photoUrls": [
      "https://pbs.twimg.com/media/DInGhjWUMAEg-qO?format=jpg&name=large"
    ],
    "tags": [
      {
        "id": 1005003,
        "name": "Ghost"
      }
    ],
    "status": "pending"
  }
]`)

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
