package easy

/*
	runtime 0 ms 100%
	memory 3 mb 100%
*/

func maximumWealth(accounts [][]int) int {
	maximumSum, tmpSum := 0, 0

	for i := 0; i < len(accounts); i++ {
		for j := 0; j < len(accounts[i]); j++ {
			tmpSum += accounts[i][j]
		}
		if maximumSum < tmpSum {
			maximumSum = tmpSum
		}
		tmpSum = 0
	}
	return maximumSum
}
