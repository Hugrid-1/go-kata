package main

import (
	"reflect"
	"testing"
	"time"
)

type fields struct {
	head *Node
	tail *Node
	curr *Node
	len  int
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
func generateList() (DoubleLinkedList, int) {
	list := DoubleLinkedList{}
	jsonFile, len, err := GenerateJSON()
	check(err)
	err = list.LoadData(jsonFile.Name())
	check(err)
	return list, len
}

func TestDoubleLinkedList_Current(t *testing.T) {

	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			want:   testList.head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	testList, _ := generateList()
	type args struct {
		n int
	}
	tests := []struct {
		name   string
		fields DoubleLinkedList
		args   args
	}{
		{
			name:   "basic_test",
			fields: testList,
			args:   args{n: 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			wantedLen := d.len - 1
			deletedNode := d.head.next
			err := d.Delete(tt.args.n)
			if err != nil {
				return
			}
			if reflect.DeepEqual(d.head.next, deletedNode) {
				t.Errorf("Delete() node not deleted ")
			}
			if d.len != wantedLen {
				t.Errorf("Delete() len = %v, want len %v", d.len, wantedLen)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	testList, len := generateList()
	testList.curr = testList.curr.next.next
	tests := []struct {
		name      string
		fields    fields
		wantedLen int
	}{
		{
			name: "basic test",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.curr,
				len:  len,
			},
			wantedLen: len - 1,
		},
		{
			name: "test when current = head",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.head,
				len:  len,
			},
			wantedLen: len - 1,
		},
		{
			name: "test when current = tail",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.tail,
				len:  len,
			},
			wantedLen: len - 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}

			err := d.DeleteCurrent()
			if err != nil {
				return
			}
			//if !reflect.DeepEqual(got, tt.want) {
			//	t.Errorf("DeleteCurrent() len = %v, want len %v", d.len, tt.wantedLen)
			//}
			if d.curr == nil {
				t.Errorf("DeleteCurrent() current element = nil")
			}
		})
	}

}

func TestDoubleLinkedList_Index(t *testing.T) {
	testList, len := generateList()
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "basic test",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.curr,
				len:  len,
			},
			want: 0,
		},
		{
			name: "test with multiple iterations next ",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.head.next.next.next,
				len:  len,
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.Index(); got != tt.want && err != nil {
				t.Errorf("Index() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	testList, _ := generateList()
	type args struct {
		n int
		c Commit
	}
	tests := []struct {
		name   string
		fields DoubleLinkedList
		args   args
	}{
		{
			name:   "basic test",
			fields: testList,
			args: args{
				n: 0,
				c: Commit{
					Message: "NewCommit",
					UUID:    "111111111",
					Date:    time.Time{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			wantedLen := d.len + 1
			err := d.Insert(tt.args.n, tt.args.c)
			check(err)
			if *d.head.data != tt.args.c || d.head == nil {
				t.Errorf("First element not equal inserted element")
			}

			if got := d.Len(); got != wantedLen {
				t.Errorf("Len() = %v, want %v", got, wantedLen)
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {

	testList, len := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   int
	}{
		{
			name:   "basic list",
			fields: testList,
			want:   len,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_LoadData(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "basicTEst",
			args: args{
				path: "data/test.json",
			},
			wantErr: false,
		},
		{
			name: "test with corrupted json",
			args: args{
				path: "data/test_corrupted.json",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{}
			if err := d.LoadData(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {

	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			want:   testList.curr.next,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			want:   testList.tail,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.Pop(); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	testList, len := generateList()

	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "basic test",
			fields: fields{
				head: testList.head,
				tail: testList.tail,
				curr: testList.head.next,
				len:  len,
			},
			want: testList.head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *DoubleLinkedList
	}{
		{
			name:   "",
			fields: testList,
			want: &DoubleLinkedList{
				head: &Node{
					data: testList.tail.data,
					prev: nil,
					next: testList.tail.prev,
				},
				tail: &Node{
					data: testList.head.data,
					prev: testList.head.next,
					next: nil,
				},
				curr: testList.curr,
				len:  testList.len,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.Reverse(); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		arg    string
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			arg:    testList.head.next.data.Message,
			want:   testList.head.next,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.Search(tt.arg); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		arg    string
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			arg:    testList.head.next.data.UUID,
			want:   testList.head.next,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.SearchUUID(tt.arg); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	testList, _ := generateList()
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			name:   "basic test",
			fields: testList,
			want:   testList.head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got, err := d.Shift(); !reflect.DeepEqual(got, tt.want) && err != nil {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
		})
	}
}
